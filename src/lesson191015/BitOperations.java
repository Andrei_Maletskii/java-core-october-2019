package lesson191015;

public class BitOperations {

    public static void main(String[] args) {
        System.out.println("--- initial");
        int a = 60;
        int b = Integer.MIN_VALUE;

        System.out.println(Integer.toBinaryString(a));
        System.out.println(Integer.toBinaryString(b));


        System.out.println("--- a1");
        int a1 = a >> 1;
        System.out.println(a1);
        System.out.println(Integer.toBinaryString(a1));

        System.out.println("--- a2");
        int a2 = a >>> 1;
        System.out.println(a2);
        System.out.println(Integer.toBinaryString(a2));

        System.out.println("--- b1");
        int b1 = b >> 1;
        System.out.println(b1);
        System.out.println(Integer.toBinaryString(b1));

        System.out.println("--- b2");
        int b2 = b >>> 1;
        System.out.println(b2);
        System.out.println(Integer.toBinaryString(b2));

        System.out.println("--- b11");
        int b11 = b1 >> 1;
        System.out.println(b11);
        System.out.println(Integer.toBinaryString(b11));

        System.out.println("--- b22");
        int b22 = b2 >>> 1;
        System.out.println(b22);
        System.out.println(Integer.toBinaryString(b22));

        System.out.println("--- b111");
        int b111 = b11 >> 1;
        System.out.println(b111);
        System.out.println(Integer.toBinaryString(b111));

        System.out.println("--- b222");
        int b222 = b22 >>> 1;
        System.out.println(b222);
        System.out.println(Integer.toBinaryString(b222));

    }

}
