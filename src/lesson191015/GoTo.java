package lesson191015;

public class GoTo {

    public static void main(String[] args) {
//        goto

        mark:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                break mark;
            }
        }

        int i = 0;
        int j = 0;

        mark:
        switch (i) {
//            case 0 -> System.out.println("0");
            case 0:
                System.out.println("0");
                switch (j) {
                    case 0:
                        System.out.println("1");
                        break mark;
                }
        }

        byte b1 = 0;
        byte b2 = 1;

        byte result = (byte) (b1 + b2);

//        goto mark;
//        const i;
    }
}
