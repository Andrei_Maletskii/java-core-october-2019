package lesson191015;

public class OperatorsUse {

    public static void main(String[] args) {
        int i = 0;

        i = i + 5;
        i += 5;

        i = i * 3;
        i *= 3;

        int j = 0;

        System.out.println(++j);
        System.out.println(j++);
        System.out.println(j);

        System.out.println(i);

        int k = 0;

        int m = k++ + k++;
        System.out.println(m);
        System.out.println(k);
    }
}
