package lesson191015;

import java.util.ArrayList;
import java.util.List;

public class ListTransition {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("a");
        list.add(new Integer(5));

        System.out.println("list1 = " + list);

        changeList(list);

        System.out.println("list2 = " + list);
    }

    private static void changeList(List list) {
        System.out.println("b1 = " + list);
        list.add("b");
        System.out.println("b2 = " + list);



        String a = "abcd";
        String substring = a.substring(1);

        System.out.println("immutable string = " + a);
        System.out.println(substring);
    }
}
