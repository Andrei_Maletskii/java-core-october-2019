package lesson191015;

public class DefaultPrimitives {

    public int i;
    public static long lng;
    String name;
    char ch;
    Character ch1;

    public static void method() {

    }

    public static void main(String[] args) {
        DefaultPrimitives defaultPrimitives = new DefaultPrimitives();

        System.out.println(defaultPrimitives.i);
        System.out.println(defaultPrimitives.lng);
        System.out.println(DefaultPrimitives.lng);

        defaultPrimitives.method();


//        int j;
//        String name1;

//        System.out.println(name1);
        System.out.println(defaultPrimitives.name);
        System.out.println(defaultPrimitives.ch);
        System.out.println(defaultPrimitives.ch1);
    }
}

class VariablesUse {
    public static void main(String[] args) {
        DefaultPrimitives defaultPrimitives = new DefaultPrimitives();
        System.out.println(defaultPrimitives.i);
        System.out.println(defaultPrimitives.lng);
        System.out.println(DefaultPrimitives.lng);


        int i;
        i = 0;
        int j = 0;

        int i1 = 10, k;

        int k1, k2 = 30;

//        System.out.println(k);
    }
}
