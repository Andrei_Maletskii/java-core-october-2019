package lesson191015;

public class Modificators {

    public static void main(String[] args) {
        AboutJava aboutJava = new AboutJava();

        aboutJava.publicInfo();
//        aboutJava.privateInfo(); ERROR
    }
//    public static void main(String args11233[]) {}
//    public static void main(String... args) {}
}

class AboutJava {

    public void publicInfo() {
        System.out.println("Public info");
        privateInfo1();
        privateInfo2();
    }

    private void privateInfo1() {
        System.out.println("Private info");
    }

    private void privateInfo2() {
        System.out.println("Private info");
    }

    void packagePrivateInfo() {
        System.out.println("PP Info");
    }
}
