package lesson191015;

import java.util.concurrent.Future;

public class PrimitiveOverflow {

    public static void main(String[] args) {

        long i = Integer.MAX_VALUE;

        System.out.println(i + 1);
    }

    public void method() {
//        return 1;
    }
}
