package lesson191209;

public class MySyncAccount {

    final Object lock = new Object();
    final Object lock1 = new Object();
    final Object lock2 = new Object();

    public synchronized void deposit1(int count) {
        // operator 1
        // operator 2
        // operator 3
    }

    public void deposit2(int count) {
        synchronized (this) {
            // operator 1
            // operator 2
            // operator 3
        }
    }

    public void deposit3(final Object lock, int count) {
        synchronized (lock) {

        }
    }

    public void deposit4(int count) {
        synchronized (lock1) {

        }
    }

    public void deposit5(int count) {
        synchronized (lock2) {

        }
    }


}
