
package lesson191209.producer.consumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingLinkedQueue {

    private final Queue<Integer> internalQueue = new LinkedList<>();

    private final int capacity;

    public MyBlockingLinkedQueue(int capacity) {
        this.capacity = capacity;
    }

    // USED BY PRODUCER
    public synchronized void put(Integer i) throws InterruptedException {
        while (internalQueue.size() >= capacity) {
            System.out.println(Thread.currentThread().getName() + " The queue is full, waiting...");
            wait();
            System.out.println(Thread.currentThread().getName() + " The queue is not full, go for it...");
        }

        internalQueue.offer(i);
        notifyAll();
    }

    // USED BY CONSUMER
    public synchronized Integer take() throws InterruptedException {
        while (internalQueue.isEmpty()) {
            System.out.println(Thread.currentThread().getName() + " The queue is empty, waiting...");
            wait();
            System.out.println(Thread.currentThread().getName() + " The queue is not empty, go for it...");
        }

        Integer integer = internalQueue.poll();
        notifyAll();
        return integer;
    }

    public static void main(String[] args) throws InterruptedException {
        MyBlockingLinkedQueue myQueue = new MyBlockingLinkedQueue(1);

        IntegerConsumer integerConsumer1 = new IntegerConsumer(myQueue);
        IntegerConsumer integerConsumer2 = new IntegerConsumer(myQueue);
        IntegerProducer integerProducer1 = new IntegerProducer(myQueue, 1000, 1000);
        IntegerProducer integerProducer2 = new IntegerProducer(myQueue, 1000, 1000);

        Thread producer1 = new Thread(integerProducer1);
        Thread producer2 = new Thread(integerProducer2);
        Thread consumer1 = new Thread(integerConsumer1);
        Thread consumer2 = new Thread(integerConsumer2);

        producer1.setName("producer1");
        producer2.setName("producer2");
        consumer1.setName("consumer1");
        consumer2.setName("consumer2");

        producer1.start();
        producer2.start();
        consumer1.start();
//        consumer2.start();

        producer1.join();
        producer2.join();

        myQueue.put(Integer.MIN_VALUE);
//        myQueue.put(Integer.MIN_VALUE);

        consumer1.join();
//        consumer2.join();

        System.out.println("Store size 1 = " + integerConsumer1.getStore().size());
//        System.out.println("Store size 2 = " + integerConsumer2.getStore().size());


    }
}
