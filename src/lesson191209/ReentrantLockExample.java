package lesson191209;

public class ReentrantLockExample {

    public static void main(String[] args) {
        B b = new B();

        b.a = new A();

        b.method();
    }
}

class A {
    public void method() {
        synchronized (this) {

        }
    }
}

class B {
    A a = new A();

    public void method() {
        synchronized (a) {
            a.method();
        }
    }
}
