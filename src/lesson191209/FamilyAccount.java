package lesson191209;

public class FamilyAccount {
    private int fatherBalance = 0;
    private int motherBalance = 0;

    final Object fatherLock = new Object();
    final Object motherLock = new Object();

    public void fatherDeposit(int amount) {
        synchronized (fatherLock) {
            fatherBalance += amount;
        }
    }

    public void motherDeposit(int amount) {
        synchronized (motherLock) {
            motherBalance += amount;
        }
    }

    public void fatherWithdraw(int amount) {
        synchronized (fatherLock) {
            fatherBalance -= amount;
        }
    }

    public void motherWithdraw(int amount) {
        synchronized (motherLock) {
            motherBalance -= amount;
        }
    }

    public void motherToFather(int amount) {
        synchronized (motherLock) {
            synchronized (fatherLock) {
                motherBalance -= amount;
                fatherBalance += amount;
            }
        }
    }

    public void fatherToMother(int amount) {
        synchronized (motherLock) {
            synchronized (fatherLock) {
                fatherBalance -= amount;
                motherBalance += amount;
            }
        }
    }

    public int getFatherBalance() {
        synchronized (fatherLock) {
            return fatherBalance;
        }
    }

    public int getMotherBalance() {
        synchronized (motherLock) {
            return motherBalance;
        }
    }
}
