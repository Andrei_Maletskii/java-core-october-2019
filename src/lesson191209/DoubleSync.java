package lesson191209;

public class DoubleSync {

    public static void main(String[] args) throws InterruptedException {
        Z z = new Z();

        Thread thread = new Thread(() -> {
            try {
                z.sleep();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread thread1 = new Thread(() -> {
            z.enterLock1();
        });

        thread.start();
        Thread.sleep(1000);
        thread1.start();


    }
}

class Z {
    private final Object lock1 = new Object();
    private final Object lock2 = new Object();

    public void sleep() throws InterruptedException {
        synchronized (lock1) {
            synchronized (lock2) {
                System.out.println("lock 2 captured, wait()");
                lock2.wait();
            }
        }
    }

    public void enterLock1() {
        synchronized (lock1) {
            System.out.println("lock1 captured!");
        }
    }
}
