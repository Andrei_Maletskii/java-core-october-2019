package lesson191209;

public class SyncExamples {

    final Object lock = new Object();
    final static Object staticLock = new Object();

    public synchronized void method1() {

    }

    public void method2() {
        synchronized (this) {

        }
    }

    public void method3() {
        synchronized (lock) {
            synchronized (staticLock) {

            }
        }
    }


    public synchronized static void staticMethod1() {

    }

    public static void staticMethod2() {
        synchronized (SyncExamples.class) {

        }
    }

    public static void staticMethod3(final Object lock) {
        synchronized (staticLock) {
            synchronized (lock) {

            }
        }
    }
}
