package lesson191209.racecondition;

import java.util.concurrent.atomic.AtomicInteger;

public class GoodCounter implements Counter {

	private AtomicInteger atomicInteger = new AtomicInteger(0);

	public int getCount() {
		return atomicInteger.get();
	}

	public void increment() {
//		atomicInteger.incrementAndGet();

		// CAS Compare and Swap
		atomicInteger.getAndIncrement();
	}
}
