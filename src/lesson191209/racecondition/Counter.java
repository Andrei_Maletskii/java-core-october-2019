package lesson191209.racecondition;

public interface Counter {

    int getCount();

    void increment() throws InterruptedException;
}
