package lesson191209.racecondition;

import java.util.concurrent.locks.ReentrantLock;

public class BadCounter implements Counter {

    private int i = 0; // need visibility and atomicity
    private ReentrantLock lock = new ReentrantLock();

    public int getCount() {
        return i;
    }

	public void increment1() {
    	synchronized (this) {
			i++;
		}
//     read i
//     i + 1
//     write i
	}

    public void increment() throws InterruptedException {
        lock.lockInterruptibly();

        i++;

        lock.unlock();
    }
}
