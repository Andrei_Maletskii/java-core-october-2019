package lesson191209.racecondition;

class CounterIncrementer implements Runnable {

	private Counter counter;
	private int n;

	public CounterIncrementer(Counter Counter, int n) {
		this.counter = Counter;
		this.n = n;
	}

	@Override
	public void run() {
		for (int i = 0; i < n; i++) {
			try {
				counter.increment();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(counter.getCount());
	}
}

public class Main {
	public static final int N = 1_000_000;

	public static void main(String[] args) throws InterruptedException {
		Counter counter = new BadCounter();

		Thread thread1 = new Thread(new CounterIncrementer(counter, N));
		Thread thread2 = new Thread(new CounterIncrementer(counter, N));

		thread1.start();
		thread2.start();

		thread1.join();
		thread2.join();

		System.out.println(counter.getCount());
	}
}
