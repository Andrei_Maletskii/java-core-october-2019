package lesson191128;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class BadExampleForArrayList {
    public static final int N = 20;
    public static final int K = 10;

    public static void main(String[] args) {
        Collection<Integer> collection = new LinkedList<>();
        for (int i = 0; i < K; i++) {
            collection.add(i);
        }


        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            list.add(i);
        }

        for (Integer integer : collection) { // DO NOT DO SO!
            list.add(0, integer);
        } // O(N*K)

        list.addAll(0, collection); // DO LIKE THAT
        // O(N+K)
    }
}
