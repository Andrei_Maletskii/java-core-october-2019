package lesson191128;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListExample {

    private static final int N = 1_000_00;

    public static void main(String[] args) {
        System.out.println("default: " +
                estimateOneMillionAdditions(new ArrayList<>()));

        System.out.println("init capacity: " +
                estimateOneMillionAdditions(new ArrayList<>(N)));

        System.out.println("linked list: " +
                estimateOneMillionAdditions(new LinkedList<>()));

//      USE  System.arraycopy();
        int[] array1 = new int[] {1, 2, 3};
        int[] array2 = new int[3];
//      System.arraycopy(array1, 0, array2, 0, array1.length);

        for (int i = 0; i < array1.length; i++) {
            array2[i] = array1[i];
        }
    }

    private static long estimateOneMillionAdditions(List<Integer> list) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
//            int size = list.size();
//            int insertIndex = size / 2;
            list.add(0, i);
        }
        long stop = System.currentTimeMillis();

        return stop - start;
    }
}
