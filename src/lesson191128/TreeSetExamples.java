package lesson191128;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetExamples {


    public static void main(String[] args) {
        TreeSet<String> comparableStrings = new TreeSet<>();
        TreeSet<String> strings = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });


        TreeSet<TestClass> objects = new TreeSet<>();
        objects.add(new TestClass());


        Set<String> set = new HashSet<>();

        set.add("one"); // INSERT
        set.remove("one"); // DELETE
        boolean isHere = set.contains("one");// SEARCH
    }
}

class TestClass  {

}
