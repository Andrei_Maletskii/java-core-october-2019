package lesson191128;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListExamples {

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();

        list.add("one");
        list.add("two");
        list.add("three");

        ListIterator<String> iterator = list.listIterator();

        while (iterator.hasNext())
            System.out.println(iterator.nextIndex() + " : " + iterator.next());


    }
}
