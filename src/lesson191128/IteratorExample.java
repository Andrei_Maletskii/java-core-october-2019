package lesson191128;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class IteratorExample {

    public static void main(String[] args) {
//        Set<String> bst = new HashSet<>() {
        Set<String> bst = new TreeSet<>() {
            {
                add("aaaaaaaa");
                add("one");
                add("three");
                add("two");
                add("four");
                add("aaaaaaab");
            }
        };

        int i = 0;
        Iterator<String> iterator = bst.iterator();
        while (iterator.hasNext()) {
            String elem = iterator.next();
            if (i == 3) {
                iterator.remove();
//                bst.remove("two");
            }

            System.out.println(elem);
            i++;
        }

//        int i = 0;
//        for (String s : bst) {
//            if (i == 3) {
//                bst.remove("two");
//            }
//            System.out.println(s);
//            i++;
//        }
    }
}
