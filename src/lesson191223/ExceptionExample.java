package lesson191223;

class MissingMoneyException extends Exception {}
class MissingFoodException extends Exception {}
class Problems {

    public void doIHaveProblems(boolean food)
            throws MissingFoodException, MissingMoneyException {
        if (food) {
            throw new MissingFoodException();
        }

        throw new MissingMoneyException();
    }
}

public class ExceptionExample {

    public static void main(String[] args)
            throws MissingFoodException, MissingMoneyException {
        Problems problems = new Problems();

        try {
            problems.doIHaveProblems(true);
        } catch (Exception e) {
            throw e;
        }
    }
}
