package lesson191223;

import java.io.Closeable;
import java.io.IOException;

class MyCloseable implements Closeable {

    public void method() {
        throw new RuntimeException();
    }

    @Override
    public void close() throws IOException {

    }
}

class MyAutoCloseable implements AutoCloseable {

    public void method() {
        throw new RuntimeException("hello");
    }

    @Override
    public void close() throws Exception {
        throw new RuntimeException("close");
    }
}
public class CloseableExample {

    public static void main(String[] args) {
        MyAutoCloseable myAutoCloseable = new MyAutoCloseable();
        MyCloseable myCloseable = new MyCloseable();

//        try (myAutoCloseable) {
//            myAutoCloseable.method();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            myAutoCloseable.method();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                myAutoCloseable.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        try (myCloseable) {
            myCloseable.method();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
