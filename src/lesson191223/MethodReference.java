package lesson191223;

import java.util.function.BiFunction;

public class MethodReference {
    public static void main(String[] args) {
        "".charAt(0);

        BiFunction<String, Integer, Character> methodRef = (String str, Integer index) -> str.charAt(index);
        BiFunction<String, Integer, Character> string = String::charAt;


    }
}
