package lesson191223;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

interface Vehicle {}
class Bus implements Vehicle {}
class SuperBus extends Bus implements Collection {
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
//class ArrayBus extends Bus, ArrayList;

public class BusExample {
    public static void main(String[] args) {
        Bus bus = new Bus();

        boolean b = null instanceof Bus;
//        boolean b1 = bus instanceof ArrayList;
        boolean b2 = bus instanceof Collection;

        SuperBus bus1 = new SuperBus();

//        boolean b3 = bus1 instanceof ArrayList;
        boolean b4 = bus1 instanceof Collection;


    }
}
