package lesson191028;

import java.util.ArrayList;
import java.util.List;

class Faculty implements Cloneable {
    private String name;
    private int numberDepartments;
    private List<Department> departmentList;

    public Object clone() throws CloneNotSupportedException {
        Faculty obj = (Faculty) super.clone();
        if (null != this.departmentList) {
            ArrayList<Department> tempList =
                    new ArrayList<>(this.departmentList.size());
            for (Department listElem : this.departmentList) {
                tempList.add(listElem.clone());
            }
            obj.departmentList = tempList;
        }
        return obj;
    }
}

