package lesson191028;

import java.util.ArrayList;

public class Mark<T extends Number> {
    public T mark;
    public Mark (T value) {
        mark = value;
    }
    public T getMark () {
        return mark;
    }
    public int roundMark () {
        return Math.round(mark.floatValue ());
    }
    /* вместо */ // public boolean sameAny (Mark<T> ob) {
    public boolean sameAny (Mark<?> ob) {
        return roundMark () == ob.roundMark ();
    }

    public boolean method(ArrayList<Integer> list) {
        Number number = list.get(0);
        return true;

    }
    public boolean same (Mark<T> ob) {
        return getMark () == ob.getMark ();
    }

    public static void main(String[] args) {
        Mark<Integer> integerMark = new Mark<>(5);

//        integerMark.same(new Mark<Double>(1.2)); ERROR
        integerMark.same(new Mark<>(10));
        integerMark.sameAny(new Mark<>(1.2));
        integerMark.sameAny(new Mark<>(1.2f));

//        integerMark.method(new ArrayList<Integer>());
//        integerMark.method(new ArrayList<Double>());
        ArrayList<Number> list = new ArrayList<>();
        list.add(5);
        list.add(1.2);
        ArrayList<Integer> intList = new ArrayList<>();

//        integerMark.method(list);
        integerMark.method(intList);
    }
}
