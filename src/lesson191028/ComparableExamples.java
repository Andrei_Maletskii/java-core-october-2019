package lesson191028;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparableExamples {

    public static void main(String[] args) {
        Account account1 = new Account(100, 200);
        Account account2 = new Account(100, 250);

//        if (account1 > account2) {
//
//        }

        System.out.println(account1.compareTo(account2));

        AccountComparator1 accountComparator1 = new AccountComparator1();

        System.out.println(accountComparator1.compare(account1, account2));


        List<Account> accountList = new ArrayList<>();

        accountList.add(account1);
        accountList.add(account2);
        accountList.add(new Account(300, 300));

        accountList.sort(accountComparator1);

        Collections.sort(accountList);
        Collections.sort(accountList, accountComparator1);

    }
}

class Account implements Comparable<Account> {
    int moneyCount1;
    int moneyCount2;

    public Account(int moneyCount1, int moneyCount2) {
        this.moneyCount1 = moneyCount1;
        this.moneyCount2 = moneyCount2;
    }

    @Override
    public int compareTo(Account that) {
        return (this.moneyCount1 + this.moneyCount2)
                - (that.moneyCount1 + that.moneyCount2);
    }
}

class AccountComparator1 implements Comparator<Account> {

    @Override
    public int compare(Account o1, Account o2) {
        return o1.moneyCount1 - o2.moneyCount1;
    }
}

class AccountComparator2 implements Comparator<Account> {

    @Override
    public int compare(Account o1, Account o2) {
        return o1.moneyCount2 - o2.moneyCount2;
    }
}
