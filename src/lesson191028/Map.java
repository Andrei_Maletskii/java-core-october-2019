package lesson191028;

public interface Map {

    void method();

    interface Entry {

    }

}

class MapImpl implements Map {
    class MapEntryImpl implements Entry {

    }

    @Override
    public void method() {

    }
}

class Impl implements Map.Entry {

}
