package lesson191028;

import java.util.ArrayList;

public class ExtendsSuperExample {

    public static void main(String[] args) {

    }

    public static void printList(ArrayList<Number> list) {
        System.out.println(list);
        Number number = list.get(0);
        list.add(1);
    }

    public static void printFirstElement(ArrayList<? extends Number> list) {
        Number number = list.get(0);
//        list.add(10); // ERROR
    }

    public static void addElement(ArrayList<? super Number> list) {
        list.add(10);
        Object object = list.get(0);
    }

    public static void copy(ArrayList<? super Number> dst,
                            ArrayList<? extends Number> src) {
//        Number number = 1;
//        src.add(number);
//        src.add(1);
//        src.add(1.2);

        for (Number number : src) {
            dst.add(number);

            Object object = dst.get(0);
        }
    }

    // PECS - Producer Extends Consumer Super
}
