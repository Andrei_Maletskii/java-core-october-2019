package lesson191028;

import java.util.Date;

public class Department implements Cloneable {
    private Integer name;
    private Date date;

    public Department(Integer name, Date date) {
        this.name = name;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public Department clone() throws CloneNotSupportedException {
        Department obj = (Department) super.clone();
        if (null != this.date) {
            obj.date = (Date) this.date.clone();
        }
        return obj;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Department department = new Department(10, new Date());

        Department clone = department.clone();

        System.out.println(department.equals(clone));
        System.out.println(department == clone);

        System.out.println(clone.name);
        System.out.println(clone.date);

        System.out.println(department.name == clone.name);
        System.out.println(department.date == clone.date);
    }
}

