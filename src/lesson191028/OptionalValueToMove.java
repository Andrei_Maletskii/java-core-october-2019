package lesson191028;

public class OptionalValueToMove<T extends Moveable> {
    private T value;

    public OptionalValueToMove(T value) {
        this.value = value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void move() {
        value.move();
//        value.changeWheels();
    }

    public static void main(String[] args) {
        OptionalValueToMove<SportCar> moveableOptionalValueToMove =
                new OptionalValueToMove<>(new SportCar());

        moveableOptionalValueToMove.move();

    }
}

interface Moveable {
    void move();
}

class Car implements Moveable {

    @Override
    public void move() {
        System.out.println("move");
    }

    public void changeWheels() {
        System.out.println("wheels changed");
    }
}

class SportCar extends Car {

}
