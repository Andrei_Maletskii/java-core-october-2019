package lesson191028;

import java.util.ArrayList;

public class GenericExample {

    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        ArrayList list1 = new ArrayList();

        ArrayList<String> strings = new ArrayList<>();
        strings.add("1");
        strings.add("2");

        list.add("hello");
        list.add(1);
        list.add(1.2);
    }


}
