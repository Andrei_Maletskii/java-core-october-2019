package lesson191024;

public class NoInheritance {

}

final class FinalClass {

}

//class FinalClassChild extends FinalClass { // ERROR
//
//}

class PrivateConstructor {

    private PrivateConstructor() {
    }
}

//class PrivateConstructorChild extends PrivateConstructor { // ERROR
//
//    public PrivateConstructorChild() {
//        super();
//    }
//}
