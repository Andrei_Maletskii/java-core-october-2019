package lesson191024;

public class MultipleInheritanceIsBad implements AbstractA1, AbstractA2 {

}

interface DefaultI1 extends DefaultI2 {
    default void method() {

    }
}

interface DefaultI2 {
    default void method() {

    }
}

interface AbstractA1 extends DefaultI1 {

}

interface AbstractA2 extends DefaultI2 {

}


