package lesson191024;

public class InitialBlockInheritance {
    public static void main(String[] args) {
        Doctor doc = new Doctor();


//        Doctor.stMan();
//        System.out.println("Run.");
//        Doctor doctor = new Doctor();
//        System.out.println(doctor.form);
//        Doctor.stDoctor();
//
//        doctor.stMan();
//        doctor.stDoctor();


    }
}

class Man {
    public static String form = "man";
    static {
        System.out.println("static block in Man.");
    }
    {
        System.out.println("logic block in Man");
    }

    public Man() {
        System.out.println("Man constructor");
    }

    public static void stMan() {
        System.out.println("static method in Man.");
    }
}

class Doctor extends Man {
    static {
        System.out.println("static block in Doctor");
    }
    {
        System.out.println("logic block in doctor");
    }

    public Doctor() {
        System.out.println("Doctor constructor");
    }

    public static void stDoctor() {
        System.out.println("static method in Doctor.");
    }
}
