package lesson191024;

public class MultipleInheritance {


}

interface CarI {

    void drive();
}

interface ComputerI {
    public static final int MHZ = 100;

    void process();

    default void turnOff() {
        turnOffFan();
        turnOffProcessor();
        System.out.println("turning off...");
    }

    private void turnOffFan() {
        System.out.println("turning off the fan...");
    }

    private void turnOffProcessor() {
        System.out.println(getMhz() + " processor is turning off...");
    }

    int getMhz();
}

abstract class AbstractComputer {

    private int mhz;

    public AbstractComputer(int mhz) {
        this.mhz = mhz;
    }

    public abstract void process();

    public void turnOff() {
        turnOffFan();
        System.out.println("turning off...");
    }

    private void turnOffFan() {
        System.out.println("turning off the fan...");
    }

    private void turnOffProcessor() {
        System.out.println(this.mhz + " processor is turning off...");
    }
}

class ModernCar implements CarI, ComputerI {

    @Override
    public void drive() {

    }

    @Override
    public void process() {

    }

    @Override
    public int getMhz() {
        return 0;
    }

    @Override
    public void turnOff() {
        System.out.println("no turnoff today");
    }
}