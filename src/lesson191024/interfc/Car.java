package lesson191024.interfc;

public class Car implements Moveable {

    @Override
    public void move() {

    }

    public static void main(String[] args) {
        Car car = new Car();
        moveSomething(car);
    }

    public static void moveSomething(Moveable something) {
        something.move();
    }
}
