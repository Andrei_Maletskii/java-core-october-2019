package lesson191024;

public class Example {

    public static void main(String[] args) {
        Derived hello1 = new Derived(10, "hello1", 77);
        Derived hello2 = new Derived(10, "hello1", 77);

        System.out.println(hello1.equals(hello2));

    }
}

class Base {
    int i;

    public Base(int i) {
        this.i = i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Base base = (Base) o;

        return this.i == base.i;
    }

    @Override
    public int hashCode() {
        return i;
    }
}

class Derived extends Base {
    String str;
    int i;

    public Derived(int i1, String str, int i2) {
        super(i1);
        this.str = str;
        this.i = i2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Derived derived = (Derived) o;

        if (this.i != derived.i) return false;
        return str != null ? str.equals(derived.str) : derived.str == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (str != null ? str.hashCode() : 0);
        return result;
    }
}
