package lesson191024;

public class CastExamples {

    public static void main(String[] args) {
        Integer integer = 5;

        Number number = integer;

        System.out.println(process(number));

        System.out.println(process(1.2));
    }

    public static int process(Number number) {
        if (number instanceof Integer) {
            Integer integer = (Integer) number;

            return integer * 2;
        }

        return -1;
    }
}
