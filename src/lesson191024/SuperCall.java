package lesson191024;

public class SuperCall {

    public static void main(String[] args) {
        B b = new B();
        b.method();
        System.out.println(b.getI());
        System.out.println(b.getSuperI());

        C c = new C();

        System.out.println(c.getI());
    }
}

class A {
    public int i;

    public A(int i) {
        this.i = i;
        System.out.println("A constructor");
    }

    public void method() {
        System.out.println("A method");
    }

    public int getI() {
        return i;
    }
}

class B extends A {
    public int i;

    public B() {
        this(5);
        this.i = 10;
        System.out.println("B constructor");
    }

    public B(int i) {
        super(i);
    }

    @Override
    public void method() {
        System.out.println("B method");
    }

    public int getSuperI() {
        return super.getI();
    }

    @Override
    public int getI() {
        return super.getI();
//        return i;
    }
}

class C extends B {

    @Override
    public int getI() {
        return super.getI();
    }
}
