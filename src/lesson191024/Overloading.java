package lesson191024;

public class Overloading {

}

class X {

    public void method1() {

    }

    public static void method2() {

    }

    public void method3() {

    }

    public static void method4() {

    }
}

class Z extends X {

//    public static void method1() { // ERROR
//
//    }

//    public void method2() { // ERROR
//
//    }

    public static final void method3(int i) { //

    }

    public void method4(int i) {

    }
}

class Y extends Z {
//    public static void method3(int i) { // ERROR
//
//    }
}
