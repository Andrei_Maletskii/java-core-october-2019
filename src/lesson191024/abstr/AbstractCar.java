package lesson191024.abstr;

public abstract class AbstractCar {

    public void move() {
        int speed = getSpeed();

        for (int i = 0; i < speed; i++) {
            System.out.println("go 1");
        }
    }

    public abstract int getSpeed();
}
