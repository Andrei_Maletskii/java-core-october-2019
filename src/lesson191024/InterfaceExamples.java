package lesson191024;

public class InterfaceExamples {

    public static void main(String[] args) {

    }
}

interface AI {
    void a();

}

interface BI extends AI {
    void b();
}

abstract class AB implements BI {

    @Override
    public void b() {

    }
}


