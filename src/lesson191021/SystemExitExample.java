package lesson191021;

public class SystemExitExample {

    public static void main(String[] args) {
        try {
            throw new RuntimeException();
        } catch (Exception e) {
            System.exit(-1);
        } finally {
            System.out.println("hello");
        }
    }
}
