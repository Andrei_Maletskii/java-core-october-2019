package lesson191021;

public class Book {

    private int pageCount;
    private int fontSize;

    public Book(int pageCount) {
        this(pageCount, 10);
    }

    public Book(int... ints) {

    }

    public Book(int pageCount, int fontSize) {
        this.pageCount = pageCount;
        this.fontSize = fontSize;
    }

    public static void main(String[] args) {
        int pageCount = 1;
        int fontSize = 2;
        new Book(pageCount, fontSize);

        int[] ints = {1, 2, 3, 4};
        new Book(ints);
        Book book = new Book(1, 2, 3, 4);
    }
}
