package lesson191021;

public class CodeConventions2 {

    public static void main(String[] args) {
        int[] array = getArray(false);

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[0]);
        }
    }

    public int devide(int target, int number) {
        if (number == 0) {
            throw new ArithmeticException("Division by zero!");
        }

        return target / number;
    }

    public static int[] getArray(boolean a) {
        if (a) {
            return new int[] {1, 2, 3};
        }
        return new int[0];
    }
}
