package lesson191021;

public class Conventions {

    public static void main(String[] args) {
        int result = method();

        System.out.println(result);


//        Line line = new Line();

//        line.x1 = 3;
//        line.x2 = 5;

    }

    private static int method() {
        return 0;
    }
}

class Line {
    private int x1, y1;
    private int x2, y2;
    private int width = 1;

    public Line(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        printCurrentWidth();
        this.width = width;
    }

    protected void method() {

    }

    private void printCurrentWidth() {
        System.out.println(this.width);
    }

}
