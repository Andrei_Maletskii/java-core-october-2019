package lesson191021;

public class Composition {
    public static void main(String[] args) {
        A a = new A();
        a.method();

        B b = new C();

    }
}

class B {
    public void method() {
        System.out.println("Hello");
    }
}

class A  {
    B b = new B();

    public void method() {
        b.method();
        b.method();
    }
}

class C extends B {

}
