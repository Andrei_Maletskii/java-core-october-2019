package lesson191021;

public class DynamicBinding {
    public static final int CONSTANT = 100;

    public static void main(String[] args) {
        DynamicBinding.useBase(new Base());
        DynamicBinding.useBase(new Base(), 5);
        useBase(new K());
        useBase(new P());
    }

    public static void useBase(Base base) {
        base.method();
    }

    public static void useBase(Base base, int times) {
        for (int i = 0; i < times; i++) {
            base.method();
        }
    }
}

class Base {
    public void method() {
        System.out.println("Base");
    }

    public Number method1(int i) {
        return i;
    }
}

class K extends Base {
    @Override
    public void method() {
        System.out.println("K");
    }

    @Override
    public Integer method1(int i) {
        return i;
    }


}

class P extends Base {
    @Override
    public void method() {
        System.out.println("P");
    }
}

