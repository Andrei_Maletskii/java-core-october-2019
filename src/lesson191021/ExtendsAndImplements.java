package lesson191021;

public class ExtendsAndImplements {
    public ExtendsAndImplements(Moveable moveable) {
        System.out.println("Moveable");
    }

    public ExtendsAndImplements(BaseCar baseCar) {
        System.out.println("base car");
    }

    public static void main(String[] args) {
        CustomCar a = new CustomCar();
        ExtendsAndImplements extendsAndImplements
                = new ExtendsAndImplements((BaseCar) a);

    }
}

interface Moveable {

    void move();
}

class BaseCar {

    public void move() {
        System.out.println("base move");
    }
}

class CustomCar extends BaseCar implements Moveable {

    @Override
    public void move() {
        System.out.println("custom move");
    }
}
