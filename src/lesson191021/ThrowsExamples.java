package lesson191021;

import java.io.IOException;
import java.sql.SQLException;

public class ThrowsExamples {

    public static void main(String[] args) {
        try {
            method();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void method() throws IOException, SQLException {
        method1();
        method2();
    }

    public static void method1() throws IOException {
        throw new IOException();
    }

    public static void method2() throws SQLException {
        throw new SQLException();
    }
}
