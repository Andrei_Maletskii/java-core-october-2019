package lesson191021;

public class TryCatch {
    public static void main(String[] args) {
        try {
            exMethod();
        } catch (Exception e) {
            // NOP
        }
    }

    public static int exMethod() {
        throw new RuntimeException("Hello");
    }
}
