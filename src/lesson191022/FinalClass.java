package lesson191022;

public final class FinalClass {

//    private final int field = 4;
    private final int field = getValue();

    private int getValue() {
        return 4;
    }

    public final void move() {
        System.out.println("move");
    }

//    public FinalClass(int field) {
//        this.field = field;
//    }

    private static final double CUSTOM_PI = 3.14;

    public static void square(final int radius) {
//        radius = 10; ERROR
        final int customRadius;

        customRadius = 4;

//        customRadius = 5; ERROR
    }

    public static void main(String[] args) {
        FinalClass.square(10);

//        FinalClass.move()
        FinalClass instance = new FinalClass();
        instance.move();
    }
}

//class MyFinalClass extends FinalClass { ERROR

//    @Override
//    public final void move() { ERROR
//
//    }
//}
