package lesson191022;

import java.util.Random;

public class Book {


    private int id;
    private int pageCount;
    private String theme;

    {
        this.id = generateId();
    }

    private static int libraryId = generateId();

    static {
        System.out.println("static init 1");
    }


    {
        System.out.println("init 2");
    }

    static {
        System.out.println("static init 2");
    }




    public Book(int pageCount, String theme) {
        this(pageCount);
        System.out.println("1 constructor");
        this.theme = theme;
    }

    public Book(int pageCount) {
        System.out.println("2 constructor");
        this.pageCount = pageCount;
        this.theme = "Default";
    }

    public static int generateId() {
        Random random = new Random();

        System.out.println("generate id");
        return random.nextInt(1000);
    }

    public static void main(String[] args) {
        Book book = new Book(100, "Fantasy");
        Book book1 = new Book(100, "Fantasy");
    }

    {
        System.out.println("init 3");
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
