package lesson191022;

public class VarArgsExample {

    public static void print(int a1, int a2, int... array) {
//        for (int i : array) {
//        }
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    public static void main(String[] args) {
        print(1, 2, 3, 4, 5);
//        print(1); ERROR
        print(1, 2);
        print(1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5);
    }
}
