package lesson191022;

public class Vehicle {

    private static Vehicle instance;

    int tireCount;

    private Vehicle(int tireCount) {
        this.tireCount = tireCount;
    }

    public static Vehicle getInstance(int tireCount) {
        if (instance == null) {
            instance = new Vehicle(tireCount);
        }
        return instance;
    }
}
