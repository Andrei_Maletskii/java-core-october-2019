package lesson191022;

public class Boat {

    private int width;
    private int length;
    private int height;

    public Boat(int width, int length, int height) {
        this.width = width;
        this.length = length;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Boat boat = (Boat) o;

        if (this.width != boat.width) return false;
        return length == boat.length;
    }

    @Override
    public int hashCode() {
        int result = width;
        result = 31 * result + length;
        result = 31 * result + height;
        return result;
    }

    @Override
    public String toString() {
        return "Boat{" +
                "width=" + width +
                ", length=" + length +
                ", height=" + height +
                '}';
    }

    public static void main(String[] args) {
        Boat boat = new Boat(10, 10, 10);

        System.out.println(boat.toString());
    }
}
