package lesson191022;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ThisReflection {

    public static void main(String[] args)
            throws IllegalAccessException,
            InvocationTargetException,
            InstantiationException,
            NoSuchMethodException {
        Class<Car> carClass = Car.class;

        Constructor<?>[] constructors = carClass.getConstructors();

        Constructor<?> constructor = constructors[0];

        Object car = constructor.newInstance();

        Method moveMethod = carClass.getDeclaredMethod("move", Integer.class);

        moveMethod.setAccessible(true);

        moveMethod.invoke(car, 5);

        Car commonCar = new Car();
    }
}

class Car {
    public Car() {
    }

    private void move(int distance) {
        this.move(Integer.valueOf(5));
        for (int i = 0; i < distance; i++) {
            System.out.println("Move by int 1");
        }
    }

    private void move(Integer distance) {
        for (int i = 0; i < distance; i++) {
            System.out.println("Move by Integer 1");
        }
    }

    public static void staticMove() {

    }
}
