package lesson191022;

public class VarArgsExamples2 {

//    public static void printArgCount(Object... args) { // 1
//        System.out.println("Object args: " + args.length);
//    }

    public static void printArgCount(Integer... args) { // 2
        System.out.println("Integer[] args: " + args.length);
    }

    public static void printArgCount(int... args) { // 3
        System.out.print("int args: " + +args.length);
    }

    public static void main(String[] args) {
        Integer[] i = { 1, 2, 3, 4, 5 };
//        printArgCount(7, "No", true, null);
//        printArgCount(i, i, i);
//        printArgCount(i, 4, 71);
//        printArgCount(i);
//        int[] ints = {5, 7};
//        Object[] objects = new Object[] {Integer.valueOf(5), Integer.valueOf(7)};
//        printArgCount(ints);
//        printArgCount(1, 2, 3, 4, 5);
    }

}
