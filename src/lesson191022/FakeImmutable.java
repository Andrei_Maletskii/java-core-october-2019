package lesson191022;

public final class FakeImmutable {

    private final Book book;

    public FakeImmutable(Book book) {
        this.book = book;
    }

    public Book getBook() {
        return book;
    }

    public static void main(String[] args) {
        FakeImmutable detective = new FakeImmutable(new Book(100, "Detective"));

        Book book = detective.getBook();

        book.setPageCount(100);

    }
}
