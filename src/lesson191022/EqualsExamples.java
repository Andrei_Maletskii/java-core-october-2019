package lesson191022;

import java.util.Objects;

public class EqualsExamples {

    public static void main(String[] args) {
        CustomBook customBook1 = new CustomBook(10);
        CustomBook customBook2 = new CustomBook(10);

        boolean equality = customBook1.equals(customBook2);

        boolean equality1 = Objects.equals(customBook1, customBook2);
    }
}

class CustomBook {
    int pageCount;

    public CustomBook(int pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomBook that = (CustomBook) o;

        return pageCount == that.pageCount;
    }

    @Override
    public int hashCode() {
        return pageCount;
    }
}
