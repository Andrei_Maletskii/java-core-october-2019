package lesson191022;

public class FinalizedExamples {
    public static void main(String[] args) {
        Finalizable finalizable;
        while (true) {
            finalizable = new Finalizable(10);
        }
    }
}

class Finalizable {
    int i;

    public Finalizable(int i) {
        this.i = i;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalized");
    }
}
