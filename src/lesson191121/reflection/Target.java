package lesson191121.reflection;

public class Target {

	private Target() {}

	private String targetMethod(boolean flag) {
		if (flag) {
			return "The boolean flag is true, congrats!";
		}

		return "The boolean flag is false, that's sad";
	}

	private String targetMethod(Boolean flag) {
		if (flag) {
			return "The Boolean flag is true, congrats!";
		}

		return "The Boolean flag is false, that's sad";
	}

	private String targetMethod(Integer integer) {
		return integer.toString();
	}
}
