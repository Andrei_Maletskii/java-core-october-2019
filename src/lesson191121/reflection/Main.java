package lesson191121.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

//    public static <T> T getRandomType(Class<T> clazz) {
//
//    }

    public static void main(String[] args) throws ClassNotFoundException,
            NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException {
//        new TargetClass().targetMethod();

        Class<?> targetClass = Class.forName("lesson191121.reflection.Target");
        Class<Target> targetClass1 = Target.class;

        System.out.println(targetClass);

        Constructor<?> declaredConstructor = targetClass.getDeclaredConstructor();

        declaredConstructor.setAccessible(true);

        Object targetObject = declaredConstructor.newInstance();

        Target target = (Target) targetObject;

        System.out.println(target);

        Class<Boolean> booleanClass = Boolean.class;
        Class<Boolean> booleanClass1 = boolean.class;
        Class<Boolean> type = Boolean.TYPE;

        System.out.println(booleanClass1 == type);
        System.out.println(booleanClass.equals(booleanClass1));

        Method targetMethod =
                targetClass.getDeclaredMethod("targetMethod", booleanClass1);

        targetMethod.setAccessible(true);

        Object result = targetMethod.invoke(target, false);

        System.out.println(result);

        targetClass.getDeclaredMethod("targetMethod", Number.class);

        Annotation[] annotations = targetClass.getAnnotations();


    }
}
