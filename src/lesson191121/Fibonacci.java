package lesson191121;

import java.math.BigInteger;

public class Fibonacci {

    private BigInteger[] cache = new BigInteger[1000];

    public BigInteger fibFast(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }


        if (n <= 1) {
            return BigInteger.valueOf(n);
        }

        if (cache[n] != null) {
            return cache[n];
        } else {
            cache[n] = fibFast(n - 1).add(fibFast(n - 2));
            return cache[n];
        }
    }

    public void clearCache() {
        cache = new BigInteger[1000];
    }
}
