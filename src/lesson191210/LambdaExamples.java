package lesson191210;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class LambdaExamples {

    public static void main(String[] args) throws Exception {
        new LambdaExamples().method();
    }

    public void method() throws Exception {
        int myResult = getResult();
        CompletableFuture<Integer> result1 = CompletableFuture.completedFuture(myResult);

        Supplier<Integer> supplier = () -> getResult();
        CompletableFuture<Integer> result2 = CompletableFuture.supplyAsync(supplier);

        Integer join = result1.join();
        Integer join1 = result2.join();

        System.out.println(join.equals(join1));

        Callable<Integer> callable1 = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println(this);

                return 17;
            }
        };

        Callable<Integer> callable2 = () -> {
            System.out.println(this);

            return 17;
        };

        callable1.call();
        callable2.call();
    }

    private static int getResult() {
        return 17;
    }
}
