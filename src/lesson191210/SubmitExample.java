package lesson191210;

import lesson191112.Fibonacci;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.*;

public class SubmitExample {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);

//        executorService.execute();
        Future<?> future = executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread.sleep(1000);

        System.out.println("is Done? " + future.isDone());

        try {
            future.get(1, TimeUnit.SECONDS);
        } catch (ExecutionException | TimeoutException e) {
            System.out.println("ex " + e);
            future.cancel(true);
        }
        System.out.println(future.isDone());

        Future<Long> futureResult = executorService.submit(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return Fibonacci.fib(10);
            }
        });

        try {
            Long result = futureResult.get();

            System.out.println(result);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        executorService.shutdown();
    }
}
