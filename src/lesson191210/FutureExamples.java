package lesson191210;

import lesson191112.Fibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.*;

public class FutureExamples {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        List<Future<Long>> futureResults = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            int j = i;
            Future<Long> futureResult = executorService
                    .submit(new Callable<Long>() {
                        @Override
                        public Long call() throws Exception {
                            return Fibonacci.fib(j);
                        }
                    });

            futureResults.add(futureResult);
        }

        List<Long> results = new ArrayList<>();

        /* for (Future<Long> futureResult : futureResults) {
            try {
                Long result = futureResult.get();
                results.add(result);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }*/

        ListIterator<Future<Long>> iterator =
                futureResults.listIterator(futureResults.size());

        while (iterator.hasPrevious()) {
            Future<Long> futureResult = iterator.previous();

            try {
                Long result = futureResult.get(10, TimeUnit.MILLISECONDS);

                results.add(result);
            } catch (ExecutionException | TimeoutException e) {
                results.add(-1L);
            }

        }

        System.out.println(results);

        executorService.shutdown();
    }
}
