package lesson191216;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class FunctionVsOperator {

    public static void main(String[] args) {
        String newStr1 = doSomething1(
                "hello, ",
                "world!",
                (str1, str2) -> str1 + str2
        );

        String newStr2 = doSomething2(
                "hello, ",
                "world!",
                (str1, str2) -> str1 + str2
        );
    }

    private static String doSomething1(String str1,
                                      String str2,
                                      BiFunction<String, String, String> biFunction) {
        return biFunction.apply(str1, str2);
    }

    private static String doSomething2(String str1,
                                      String str2,
                                      BinaryOperator<String> operator) {
        return operator.apply(str1, str2);
    }
}

