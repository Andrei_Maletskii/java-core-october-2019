package lesson191216;

import java.util.HashMap;
import java.util.Map;

public class MapFunctionalInterfaceExample {

    public static void main(String[] args) {
        Map<String, String> dictionary = new HashMap<>();

        dictionary.put("good morning", "доброе утро");
        dictionary.put("доброе утро", "guten tag");
        dictionary.put("hello", "halo");

        System.out.println(dictionary);

//        for (Map.Entry<String, String> entry : dictionary.entrySet()) {
//            entry.setValue(entry.getKey() + entry.getValue().toUpperCase());
//        }

        dictionary.replaceAll((key, value) -> {
            System.out.println("Old key = " + key + " old value = " + value);

            return key + value.toUpperCase();
        });

        System.out.println(dictionary);
    }
}
