package lesson191216;

import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class BiFunctionalInterfaces {

    public static void main(String[] args) {
        BiConsumer<String, Integer> biConsumer =
                (str, integer) -> System.out.println(str + integer);

        BiPredicate<String, Integer> biPredicate = (str, integer) -> {
            int anInt = Integer.parseInt(str);

            return integer.equals(anInt);
        };

        System.out.println(biPredicate.test("3", 3));
        System.out.println(biPredicate.test("6", 4));
        System.out.println(biPredicate.test("hello", 3));

        UnaryOperator<String> unaryOperator = String::toUpperCase;

        BinaryOperator<String> concat = String::concat;
    }
}
