package lesson191216;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class RemoveIfExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");

        Iterator<String> iterator = list.iterator();

//        while (iterator.hasNext()) {
//            String element = iterator.next();
//            if (element.length() > 4) {
//                iterator.remove();
//            }
//        }

        list.removeIf(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() > 4;
            }
        });

        list.removeIf(s -> s.length() > 4);

        System.out.println(list);
    }
}
