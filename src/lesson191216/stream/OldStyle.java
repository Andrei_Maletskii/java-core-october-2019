package lesson191216.stream;

import lesson191216.person.Computer;
import lesson191216.person.Person;
import lesson191216.person.Processor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OldStyle {

    public static void main(String[] args) {
        List<Person> list = getPersonList();

        // Stream API
        // ETL
        // 1) get stream
        // 2) transform
        // 3) terminal action

        List<String> processorNames = list.stream()
                .map(Person::getComputer)
                .map(Computer::getProcessor)
                .map(Processor::getName)
                .collect(Collectors.toList());

        List<String> names = list.stream()
                .map(Person::getName)
                .collect(Collectors.toList());
    }

    private static void printPersonsInfo(List<Person> personList) {
        List<String> names = new ArrayList<>();

        for (Person person : personList) {
            names.add(person.getName());

            System.out.println(person.getAge());

            String name = person.getComputer().getProcessor().getName();

            System.out.println(name);
        }
    }

    private static List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();

        personList.add(new Person(
                new Computer(new Processor("Intel i9")), "Amy", 20)
        );
        personList.add(new Person(
                new Computer(new Processor("Intel i7")), "Fedor", 22)
        );
        personList.add(new Person(
                new Computer(new Processor("Intel i5")), "Foma", 26)
        );
        personList.add(new Person(
                new Computer(new Processor("Intel i3")), "Ivan", 32)
        );
        personList.add(new Person(
                new Computer(new Processor("AMD Ryzen")), "Amy", 26)
        );

        return personList;
    }
}
