package lesson191216.person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ComparePerson {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();

        Computer computer = new Computer(null);
        personList.add(new Person(computer, "Amy", 46));
        personList.add(new Person(computer, "Amy", 26));
        personList.add(new Person(computer, "Ivan", 27));
        personList.add(new Person(computer, "Vladimir", 44));
        personList.add(new Person(computer, "Fedor", 18));

        personList.sort(Comparator.comparing(Person::getName)
                .thenComparingInt(Person::getAge));

        Arrays.asList("one", "two", "three").sort(Comparator.reverseOrder());

        System.out.println(personList);
    }
}
