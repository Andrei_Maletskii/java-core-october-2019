package lesson191216.person;

public class Person {

    private Computer computer;
    private String name;
    private int age;



    public Person(Computer computer, String name, int age) {
        this.computer = computer;
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Computer getComputer() {
        return computer;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
