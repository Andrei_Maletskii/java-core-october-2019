package lesson191216;

import lesson191216.car.old.Oil;

import java.util.function.Function;

public class FIDefaultMethods {

    public static void main(String[] args) {
        Function<Integer, String> function = Object::toString;
        // args -> new Class(args)
        Function<String, Oil> oilFunction = Oil::new;

        Function<Integer, Oil> integerOilFunction = function.andThen(oilFunction);


    }
}
