package lesson191216;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class ExprMethodReference {

    static Set<String> staticCurrentSet = new HashSet<>();

    public static void main(String[] args) {
        HashSet<Object> currentSet = new HashSet<>();

        List<String> list = new ArrayList<>();

        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");

//        HashSet<String> stringSet = new HashSet<>(list);
//        currentSet.addAll(list);

        for (String str : list) {
            currentSet.add(str);
        }

        // (args) -> expr.method(args)
        list.forEach(str -> currentSet.add(str));

        list.forEach(currentSet::add);

        list.forEach(getMySet()::add);
        list.forEach(staticCurrentSet::add);


    }

    private static Set<String> getMySet() {
        return new HashSet<>();
    }
}
