package lesson191216.car;

import lesson191216.car.old.Car;
import lesson191216.car.old.Engine;
import lesson191216.car.old.Oil;
import lesson191216.car.old.Roof;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class CarUse {

    public static void main(String[] args) {
//        driveCarNewStyle(null);
//        driveCarNewStyle(new Car());
        driveCarNewStyle(new Car(new Engine()));
        System.out.println("-----");
        driveCarNewStyle(new Car(new Engine(new Oil())));
        System.out.println("-----");
        driveCarNewStyle(new Car(new Engine(new Oil("Motul"))));

        System.out.println(whatIsYouCarOil(new Car(new Engine(new Oil("Mobil")))));
        System.out.println(whatIsYouCarOil(new Car()));
        System.out.println(whatIsYouCarOil(null));

        Car car = new Car();
        Optional<Roof> roof = car.getRoof();

        Optional<Car> optionalCar = Optional.ofNullable(new Car());

        Optional<Optional<Roof>> roof1 = optionalCar.map(Car::getRoof);
        Optional<Roof> roof2 = optionalCar.flatMap(Car::getRoof);

        Optional<Roof> roof4 = Optional.ofNullable(new Roof());
        Optional<Optional<Roof>> roof41 = Optional.of(roof4);
        Optional<Optional<Optional<Roof>>> roof411 = Optional.of(roof41);
        Optional<Roof> roof5 = roof411.flatMap(r -> r).flatMap(r -> r);

        Optional<Car> car1 = Optional.of(car);
        Optional<Engine> engine = car1.map(Car::getEngine);
        Optional<Optional<Roof>> roof6 = car1.map(Car::getRoof);
        Optional<Roof> roof7 = car1.flatMap(Car::getRoof);

        if (roof2.isPresent()) {
            Roof roof3 = roof2.get();
        }

        Roof roof3 = roof2.orElse(new Roof());

        Car car2 = new Car();
        Engine engine1 = car2.getEngine();
        Optional<Roof> roof8 = car2.getRoof();

//        String brand = optionalBrand.get();
    }

    private static String whatIsYouCarOil(Car car) { //ETL - Extract Transform Load
        return Optional.ofNullable(car) // get Source
                .map(Car::getEngine) // tranform 1
                .map(Engine::getOil) // tranform 2
                .map(Oil::getBrand) // tranform 3
                .orElse("NoName"); // load
    }

    private static void driveCarOldStyle(Car car) {
        if (car == null) {
            throw new RuntimeException("No car");
        }

        Engine engine = car.getEngine();

        if (engine == null) {
            throw new RuntimeException("No engine");
        }

        Oil oil = engine.getOil();

        if (oil == null) {
            System.out.println("Drive not to far...");
            return;
        }

        System.out.println("Drive far...");

        String brand = oil.getBrand();

        if (brand == null) {
            System.out.println("NoName oil, please change");
            return;
        }

        System.out.println("Brand: " + brand);
    }

    private static void driveCarNewStyle(Car car) {
        Optional<Car> optionalCar = Optional.of(car);

        // arg -> arg.method()
        Optional<Engine> optionalEngine = optionalCar.map(Car::getEngine);

//        if (optionalEngine.isEmpty()) {
//            throw new RuntimeException("No engine");
//        }

//        if (!optionalEngine.isPresent()) {
//            throw new RuntimeException("No engine");
//        }

        Engine engine = optionalEngine.orElseThrow(() -> new RuntimeException("No engine"));

        Optional<Oil> optionalOil = optionalEngine.map(Engine::getOil);

        optionalOil.ifPresentOrElse(
                oil -> {
                    System.out.println("Drive far...");

                    Optional<String> optionalBrand = Optional.ofNullable(oil.getBrand());

//                    String brand = optionalBrand.orElse("NoName");
                    String brand = optionalBrand.orElseGet(CarUse::calculateBrand);
//                    String brand = optionalBrand.orElseGet(() -> calculateBrand());

                    System.out.println("Brand: " + brand);

                },
                () -> System.out.println("Drive not too far...")
        );
    }

    private static String calculateBrand() {
        return "NoName";
    }
}
