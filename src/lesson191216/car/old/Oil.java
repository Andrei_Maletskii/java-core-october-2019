package lesson191216.car.old;

public class Oil {
    private String brand;

    public Oil(String brand) {
        this.brand = brand;
    }

    public Oil() {
    }

    public String getBrand() {
        return brand;
    }
}
