package lesson191216.car.old;

public class Engine {

    private Oil oil;

    public Engine(Oil oil) {
        this.oil = oil;
    }

    public Engine() {
    }

    public Oil getOil() {
        return oil;
    }
}
