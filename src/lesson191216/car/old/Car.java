package lesson191216.car.old;

import java.util.Optional;

public class Car {

    private Engine engine;
    private Roof roof;

    public Car(Engine engine) {
        this.engine = engine;
    }

    public Car(Engine engine, Roof roof) {
        this.engine = engine;
        this.roof = roof;
    }

    public Car(Roof roof) {
        this.roof = roof;
    }

    public Car() {
    }

    public Engine getEngine() {
        return engine;
    }

    public Optional<Roof> getRoof() {
        return Optional.ofNullable(roof);
    }
}
