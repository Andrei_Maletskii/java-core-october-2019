package lesson191216;

import java.util.function.*;

public class PrimitiveFuncionalInterfaces {

    public static void main(String[] args) {
        DoubleConsumer doubleConsumer = System.out::println;
        IntConsumer intConsumer;

        ObjIntConsumer<String> objIntConsumer = (str, count) -> {
            for (int i = 0; i < count; i++) {
                System.out.println(str);
            }
        };

        IntFunction<Integer> intFunction = i -> i; // redundant

    }
}
