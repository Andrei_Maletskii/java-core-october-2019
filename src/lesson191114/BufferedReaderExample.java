package lesson191114;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.IntSummaryStatistics;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BufferedReaderExample {

    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("input1.txt");

        BufferedReader br = new BufferedReader(fr);

//        String s;
//        while ((s = br.readLine()) != null) {
//            System.out.println(s);
//        }

        IntSummaryStatistics stat = br.lines()
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String s) {
                        String[] split = s.split(" : ");

                        return split[1];
                    }
                })
                .mapToInt(Integer::parseInt)
                .summaryStatistics();

        System.out.println(stat);

        br.close();

    }
}
