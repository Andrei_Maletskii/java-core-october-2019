package lesson191114;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PredefinedStreams {

    public static void main(String[] args) throws IOException {
        OutputStream stdout = System.out;
        stdout.write(104); // ASCII 'h'
        stdout.flush();
        stdout.write('\n');

        byte[] b1 = new byte[5];
        InputStream stdin1 = System.in;
        stdin1.read(b1);
        System.out.write(b1);
        System.out.write('\n');
        System.out.flush();

        InputStream stdin2 = System.in;
        byte[] b2 = new byte[stdin2.available()];

        stdin2.read(b2);

        for (byte b : b2) {
            System.out.print((char) b);
        }
        System.out.flush();

    }
}
