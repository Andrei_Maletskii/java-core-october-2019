package lesson191114;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ScannerFile {

    public static void main(String[] args) throws FileNotFoundException {
        FileReader fr = new FileReader("preparedInput");
        Scanner scan = new Scanner(fr);


        while (scan.hasNext()) {
            if (scan.hasNextInt())
                System.out.println(scan.nextInt() + ":int");
            else if (scan.hasNextDouble())
                System.out.println(scan.nextDouble() + ":double");
            else if (scan.hasNextBoolean())
                System.out.println(scan.nextBoolean() + ":boolean");
            else {
                String next = scan.next();
                if ("Exit".equals(next)) {
                    break;
                }
                System.out.println(next + ":String");
            }

        }
    }
}
