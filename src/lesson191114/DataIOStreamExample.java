package lesson191114;

import java.io.*;

public class DataIOStreamExample {

    public static void main(String[] args) throws IOException {
        writeNumbers();
        readNumbers();
    }

    private static void readNumbers() throws IOException {
        DataInputStream dis = new DataInputStream(
                new FileInputStream(
                        "primitives"
                )
        );

        for (int i = 0; i < 10; i++) {
            System.out.println(dis.readDouble());
        }

        dis.close();
    }

    private static void writeNumbers() throws IOException {
        DataOutputStream dos = new DataOutputStream(
                new FileOutputStream(
                        "primitives"
                )
        );

        dos.writeInt(0);
        dos.writeInt(1);
        dos.writeInt(2);
        dos.writeInt(3);
        dos.writeInt(4);

        dos.close();

        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(
                        "sprimitives"
                )
        );

        oos.writeObject(0);
        oos.writeObject(1);
        oos.writeObject(2);
        oos.writeObject(3);
        oos.writeObject(4);

        oos.close();
    }
}
