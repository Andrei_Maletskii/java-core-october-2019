package lesson191114;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class WriteToFileWithEncoding {

    public static void main(String[] args) throws IOException {
        PrintWriter out = new PrintWriter( // variant 1
                new BufferedWriter(
                        new OutputStreamWriter(
                                new FileOutputStream("outfilename"),
                                StandardCharsets.UTF_8)
                )
        );

        PrintWriter pw = new PrintWriter(
                "outfilename",
                StandardCharsets.UTF_16
        ); // variant 2


        pw.close();
        out.println("asdf");
        out.close();

    }
}
