package lesson191114;


import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            String next = scanner.next();
            if ("Exit".equals(next)) {
                break;
            }
            int i = scanner.nextInt();

            for (int j = 0; j < i; j++) {
                System.out.println(next);
            }
        }

        scanner.close();
    }
}
