package lesson191017;

public class OOPStart {
    public static void main(String[] args) {
        Child child = new Child(10);
        System.out.println(child.age);
        String word = child.sayWordLoudly("hello");
        child.sayHello("!");
    }
}

abstract class Parent {
    int age;

    public String sayWordLoudly(String word) {
        if (word.contains("hel")) {
            return word + "!";
        }
        return word + "?";
    }

    public final void sayHello(String postfix) {
        if (postfix.contains("!")) {
            return;
        }
        System.out.println("hello!");
    }
}

class Child extends Parent {
    public Child(int age) {
        this.age = age;
    }
}
