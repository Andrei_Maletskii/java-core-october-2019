package lesson191017;

public class BadBreak {

    public static void main(String[] args) {
        Outer:
        for (int i = 0; i < 10; i++) {
            System.out.println("i = " + i);
            for (int j = 0; j < 10; j++) {
                System.out.println("j = " + j);
                break Outer;
//                continue Outer;
            }
        }
        System.out.println("Exit");
    }
}
