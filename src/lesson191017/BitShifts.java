package lesson191017;

public class BitShifts {

    public static void main(String[] args) {
        int a = 60;
        System.out.println(Integer.toBinaryString(a));

        int b = -60;
        System.out.println(Integer.toBinaryString(b));

        int nil = 0;
        System.out.println(Integer.toBinaryString(nil));

        int min = Integer.MIN_VALUE;
        System.out.println(Integer.toBinaryString(min));


        System.out.println("a >> 1 " + Integer.toBinaryString(a >> 1));
        System.out.println("a >>> 1 " + Integer.toBinaryString(a >>> 1));


        System.out.println("min >> 1 " + Integer.toBinaryString(min >> 1));
        System.out.println("min >>> 1 " + Integer.toBinaryString(min >>> 1));

    }
}
