package lesson191017;

public class InstanceOfOperator {

    public static void main(String[] args) {
        Integer i = 10;

        System.out.println(i instanceof Integer);
        System.out.println(i instanceof Number);
        System.out.println(i instanceof Object);
        System.out.println(i instanceof Comparable);
    }
}
