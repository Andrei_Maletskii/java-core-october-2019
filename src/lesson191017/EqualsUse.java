package lesson191017;

public class EqualsUse {

    private int count = 0;
    private String name = "asd";

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        String str11 = args[0].intern();
        String str22 = args[1].intern();

        System.out.println(str11.equals(str22));
        System.out.println(str11 == str22);
    }
}
