package lesson191017;

public class LogicOperators {

    public static void main(String[] args) {
        boolean a = true;

        a ^= true;

        System.out.println(a);
    }

    private static boolean method() {
        System.out.println("return false");
        return false;
    }


}
