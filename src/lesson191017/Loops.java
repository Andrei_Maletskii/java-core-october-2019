package lesson191017;

import java.util.List;

public class Loops {

    public static void main(String[] args) {

        while (true) {
            if (getExpr()) {
                break;
            }
        }

        for (int i = 0; i < 10; i++) {

        }

        for (;;) {
            break;
        }

        Outer:
        for (int i = 0; i < 10; i++) {
            Inner:
            for (int j = 0; j < 10; j++) {
                for (int k = 0; k < 10; k++) {
                    break Inner;
                }
                break Outer;
            }
        }

        List<Integer> integers = List.of(1, 2, 3);
        for (int i = 0; i < 2; i++) {
            Integer num = integers.get(i);

            if (num == 1) {
                continue;
            }

            System.out.println(num * 2);
        }
    }

    private static boolean getExpr() {
        for (;;) {
            return true;
        }
    }
}
