package lesson191017;

public class StringUse {

    public static void main(String[] args) {
        String s = "hello ";
        int i = 5;
        State state = new State();

        String result = s + i + " " + state;

        System.out.println(state);

        System.out.println(result);
    }
}

class State {
    int i = 10;

    @Override
    public String toString() {
        return "state = " + i;
    }
}
