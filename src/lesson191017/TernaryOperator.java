package lesson191017;

public class TernaryOperator {

    public static void main(String[] args) {

        int a = method() ? 5 : 10;

        System.out.println(a);
    }

    private static boolean method() {
        System.out.println("---");
        return false;
    }
}
