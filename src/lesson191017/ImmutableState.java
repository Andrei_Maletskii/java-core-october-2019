package lesson191017;


/**
 * IMMUTABLE
 */
public final class ImmutableState {

    final int seconds;

    public ImmutableState(int seconds) {
        this.seconds = seconds;
    }

    public ImmutableState combineState(ImmutableState state) {
        return new ImmutableState(this.seconds + state.seconds);
    }

    public static void main(String[] args) {
        ImmutableState immutableState = new ImmutableState(10);


    }
}

/*
class MutableState extends ImmutableState { //ERROR

    int days;

    public MutableState(int seconds) {
        super(seconds);
    }
}
*/


