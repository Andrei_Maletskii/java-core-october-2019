package lesson191217;

import java.util.stream.Stream;

public class StreamLifeCycle {

    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);

        integerStream.forEach(System.out::println);
        integerStream.limit(3).forEach(System.out::println);


    }
}
