package lesson191217;

import java.util.stream.Stream;

public class BadStreamUse {

    public static void main(String[] args) {
        Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5);

//        intStream.skip(1);
//          WRONG!
//        intStream.limit(3);

        intStream.forEach(System.out::println);

        System.out.println("---");
        printTenNumbers(true);
        System.out.println("---");
        printTenNumbers(false);
    }


    private static void printTenNumbers(boolean isEven) {
        Stream<Integer> stream = Stream.iterate(0, i -> ++i);

        if (isEven) {
            stream = stream.filter(i -> i % 2 == 0);
        }

        stream.limit(10).forEach(System.out::println);
    }
}
