package lesson191217;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

public class AdditionalOperations {

    public static void main(String[] args) {
        long count = getDefaultStream().filter(i -> i < 5)
                .peek(System.out::println)
                .count();


//        getDefaultStream().max(Comparator.comparingInt(i -> i));
//        getDefaultStream().max(Comparator.naturalOrder());
        Optional<Integer> max = getDefaultStream().max(Integer::compareTo);
        Optional<Integer> min = getDefaultStream().min(Integer::compareTo);

        Optional<Integer> any = getDefaultStream().findAny();
        Optional<Integer> first = getDefaultStream().findFirst();

        System.out.println(any.get());
        System.out.println(first.get());

        boolean allMatch = getDefaultStream().allMatch(i -> i > -1);
        boolean anyMatch = getDefaultStream().anyMatch(i -> i >= 9);
        boolean noneMatch = getDefaultStream().noneMatch(i -> i > 1000);

        System.out.println("allMatch = " + allMatch);
        System.out.println("anyMatch = " + anyMatch);
        System.out.println("noneMatch = " + noneMatch);

        Stream<Integer> parallelStream = getDefaultStream().parallel();
        boolean isParallel = parallelStream.isParallel();
        Stream<Integer> sequentialStream = parallelStream.sequential();
        System.out.println(isParallel);
        sequentialStream.forEach(System.out::println);
    }

    private static Stream<Integer> getDefaultStream() {
//        return Stream.of(1,2,3,4,5);
        return Stream.iterate(0, i -> ++i).limit(10);
    }
}
