package lesson191217;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static lesson191217.MapAndCollectExamples.getDefaultPersonList;

public class ReduceExample {

    public static void main(String[] args) {
        Optional<Integer> sum = Stream.of(1, 2, 3, 4, 5).reduce(Integer::sum);

        if (sum.isPresent()) {
            System.out.println(sum.get());
        }


        List<Person> personList = getDefaultPersonList();


        Optional<Person> result = personList.stream().reduce((p1, p2) -> {
            int compareResult = p1.getName().compareTo(p2.getName());

            if (compareResult > 0) {
                return p1;
            }

            return p2;
        });

        if (result.isPresent()) {
            System.out.println(result.get());
        }


        Stream<Integer> empty1 = Stream.empty();
        Integer newSum = Stream.<Integer>empty().reduce(0, Integer::sum);
        Integer reduceResult = Stream.of(1, 2, 3).reduce(0, Integer::sum);

        System.out.println(newSum);
        System.out.println(reduceResult);

        Integer countOfAnimals = personList.stream()
                .reduce(
                        0,
                        (partialSum, person) -> partialSum + person.getAnimals().size(),
//                        (partialSum1, partialSum2) -> partialSum1 + partialSum2
                        Integer::sum
                );

        System.out.println(countOfAnimals);

        Integer personCount = personList.stream().reduce(
                0,
                (partialCount, person) -> partialCount + 1,
                Integer::sum
        );

        System.out.println(personCount);

        List<Object> objects1 = List.of();
        List<Integer> integers1 = List.of();

        ArrayList<Object> objects = new ArrayList<>();
        ArrayList<Integer> integers = new ArrayList<>();

        new ArrayList<Integer>().add(1);




    }
}
