package lesson191217;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static lesson191217.MapAndCollectExamples.getDefaultPersonList;

public class PrimitiveStreams {

    public static void main(String[] args) {
        IntStream zeroToNine = IntStream.range(0, 10);
        IntStream zeroToTen = IntStream.rangeClosed(0, 10);

//        int sum = zeroToNine.sum();

        Stream<Integer> boxed = zeroToNine.boxed();

        IntStream intStream = boxed.mapToInt(i -> i);

        List<Person> personList = getDefaultPersonList();

        IntStream ageStream = personList.stream()
                .map(Person::getAnimals)
                .map(animals -> animals.stream().mapToInt(Animal::getAge))
                .flatMapToInt(i -> i);

        IntSummaryStatistics intSummaryStatistics = ageStream.summaryStatistics();
//        intStream.sum()
//        intStream.average()
//        intStream.max()
//        intStream.min()
//        intStream.count()

        System.out.println(intSummaryStatistics);

    }
}
