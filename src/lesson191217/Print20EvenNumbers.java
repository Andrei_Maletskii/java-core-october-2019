package lesson191217;

import java.util.stream.Stream;

public class Print20EvenNumbers {

    public static void main(String[] args) {
        Stream<String> empty = Stream.empty();

        // get stream from source / generate stream
        Stream<Integer> stream = Stream.iterate(0, i -> i + 2);

        // apply transformations
        Stream<Integer> limit = stream.limit(20);

        // apply terminal action
        limit.forEach(System.out::println);


        Stream.iterate(0, i -> i + 2)
                .skip(1)
                .limit(20)
                .forEach(System.out::println);
    }
}
