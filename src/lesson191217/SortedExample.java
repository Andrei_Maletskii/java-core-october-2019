package lesson191217;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static lesson191217.MapAndCollectExamples.getDefaultPersonList;

public class SortedExample {
    public static void main(String[] args) {

        List<Person> personList = getDefaultPersonList();

//        Collections.sort(personList, Comparator.comparing(Person::getName));
//        personList.sort(Comparator.comparing(Person::getName));

        List<Person> sortedPersonList = personList.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());

        List<Person> sortedPersonList1 = personList.stream()
//                .sorted(Comparator.comparing(Person::getName).thenComparing())
//                .filter()
//                .skip()
//                .limit()
                .sorted(Comparator.comparing(
                        Person::getCar,
                        Comparator.comparing(Car::getBrand)
                ))
                .collect(Collectors.toList());

        System.out.println(personList);
        System.out.println(sortedPersonList);
    }
}
