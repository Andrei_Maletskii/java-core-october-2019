package lesson191217;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapAndCollectExamples {
    public static void main(String[] args) {
        List<Person> personList = getDefaultPersonList();

        List<Car> cars = personList.stream() // get stream from source
                .map(Person::getCar) // apply transform
                .collect(Collectors.toList());
//                .collect(Collectors.toSet());

        List<String> uniqueBrands = personList.stream()
                .map(Person::getCar) // first way
                .map(Car::getBrand)
                .distinct()
                .collect(Collectors.toList());

        Set<String> brandsSet = personList.stream()
//                .map(Person::getCar)
//                .map(Car::getBrand)
                .map(person -> person.getCar().getBrand()) // second way
                .collect(Collectors.toSet());

        String joinedBrands = personList.stream()
                .map(Person::getCar)
                .map(Car::getBrand)
                .distinct()
//                .collect(Collectors.toCollection())
                .collect(Collectors.joining(","));

        System.out.println(joinedBrands);


//        Map<String, Car> map = personList.stream()
//                .collect(Collectors.toMap(
//                        Person::getName,
//                        Person::getCar
//                ));

//        System.out.println("toMap1 : " + map);

        Map<String, Car> mapWithoutCollisions = personList.stream()
                .collect(Collectors.toMap(
                        Person::getName,
                        Person::getCar,
                        (car1, car2) -> car1
                ));

        System.out.println("toMap2 : " + mapWithoutCollisions);

        TreeMap<String, Car> treeMap = personList.stream()
                .collect(Collectors.toMap(
                        Person::getName,
                        Person::getCar,
                        (car1, car2) -> car1,
                        TreeMap::new
                ));

        System.out.println("toMap3 : " + treeMap);

        StringBuilder lambdaSb = personList.stream().collect(
                () -> new StringBuilder(),
                (stringBuilder, obj) -> stringBuilder.append(obj),
                (sb1, sb2) -> sb1.append(sb2)
        );

        StringBuilder simpleSb = personList.stream().collect(
                StringBuilder::new,
                StringBuilder::append,
                StringBuilder::append
        );

        System.out.println(lambdaSb.toString());

        // 1st stream - Bobik
        // 2nd stream - Sharik Belka
        // 3nd stream - Strelka

        String names = personList.stream()
//                .flatMap(person -> person.getAnimals().stream())
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .map(Animal::getName)
                .collect(Collectors.joining(","));

        System.out.println(names);

        Map<Car, List<Person>> map = personList.stream()
                        .collect(Collectors.groupingBy(Person::getCar));

        System.out.println(map);
    }

    private static Stream<Integer> getDefaultStream() {
        return Stream.of(1,2,3,4,5,6,7,8,9,10);
    }

    public static List<Person> getDefaultPersonList() {
        return Arrays.asList(
                new Person(
                        "Amy",
                        new Car("Toyota"),
                        Arrays.asList(new Animal("Bobik", 1))
                ),
                new Person(
                        "Bob",
                        new Car("Toyota"),
                        Arrays.asList(new Animal("Sharik", 10), new Animal("Belka", 5))
                ),
                new Person(
                        "Amy",
                        new Car("Volga"),
                        Arrays.asList(new Animal("Strelka", 15))
                )
        );
    }
}

class Person {
    private String name;
    private Car car;
    private List<Animal> animals;

    public Person(String name, Car car, List<Animal> animals) {
        this.name = name;
        this.car = car;
        this.animals = animals;
    }

    public String getName() {
        return name;
    }

    public Car getCar() {
        return car;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Car {
    private String brand;

    public Car(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return brand != null ? brand.equals(car.brand) : car.brand == null;
    }

    @Override
    public int hashCode() {
        return brand != null ? brand.hashCode() : 0;
    }
}

class Animal {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}




