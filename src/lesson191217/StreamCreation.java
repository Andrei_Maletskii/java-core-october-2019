package lesson191217;

import lesson191216.car.old.Car;

import java.util.*;
import java.util.stream.Stream;

public class StreamCreation {

    private static Random random = new Random();

    public static void main(String[] args) {
        Stream<Object> stream = new ArrayList<>().stream();

        new HashMap<>().keySet().stream();
        new HashMap<>().values().stream();
        new HashMap<>().entrySet().stream();

        Stream<Integer> stream1 = Arrays.stream(new Integer[]{1, 2, 3, 4, 5});

        Stream<Integer> iterate = Stream.iterate(0, i -> ++i).limit(100);
        // limit infinite stream

        iterate.forEach(System.out::println);

        System.out.println("-----");

        Stream.iterate(0, i -> i < 100, i -> ++i).forEach(System.out::println);


        System.out.println("-----");
        Stream.iterate(0, i -> ++i)
                .filter(i -> {
                    return i % 2 == 0;
                }).limit(20).forEach(System.out::println);


        Stream<Integer> integerStream = Stream.of(1);
        Stream<Integer> integerStream1 = Stream.of(1, 2, 3, 4, 5);

        long count = Stream.ofNullable(null).count();

        System.out.println(count);


        System.out.println("---");

        Stream.generate(StreamCreation::getRandomInteger)
                .limit(10)
                .forEach(System.out::println);

        Stream<Integer> oneToFive = Stream.of(1, 2, 3, 4, 5);
        Stream<Integer> six = Stream.of(6);

        Stream.concat(oneToFive, six).forEach(System.out::println);

        // 0
        // 0 + 2
        // 2 + 2

        Stream<Integer> even = Stream.iterate(0, i -> i + 2);
        Stream<Integer> notEven = Stream.iterate(1, i -> i + 2);

        System.out.println("----");

        Stream.concat(notEven, even).limit(20).forEach(System.out::println);

        System.out.println("----Builder use-----");

        Stream.Builder<Integer> builder = Stream.builder();

        for (int i = 0; i < 5; i++) {
            builder.accept(i);
        }

        builder.add(1).add(2).add(3).add(4).add(5);

        Stream<Integer> builtStream = builder.build();

        builtStream.distinct().forEach(System.out::println);


        Optional<Car> optionalCar = Optional.ofNullable(new Car());
        Stream<Car> stream2 = optionalCar.stream();
    }

    private static Integer getRandomInteger() {
        return random.nextInt(1000);
    }
}
