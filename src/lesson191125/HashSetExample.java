package lesson191125;

import java.util.HashSet;

public class HashSetExample {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>(20);

        set.add(10);
        set.add(15);
        set.add(23);
        set.add(52);
        set.add(75);
        set.add(101);

        System.out.println(set.size());

        System.out.println();
    }
}
