package lesson191125;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class IterableExample {

    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("one");
        strings.add("two");
        strings.add("three");
        strings.add("one");
        strings.add("three");
        strings.add("heelloooo");

        Iterable<String> iterable = strings;

        Iterator<String> iterator = iterable.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        for (String s : iterable) {
//
//        }

    }
}
