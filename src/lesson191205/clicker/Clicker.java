package lesson191205.clicker;

public class Clicker extends Thread {
    long click = 0;
    private volatile boolean running = true;

    public Clicker() {
    }

    public void run() {
        while (running) {
            click++;
            if (click == Long.MAX_VALUE) {
                stopClick();
            }
        }
    }

    public void stopClick() {
        running = false;
    }
}

