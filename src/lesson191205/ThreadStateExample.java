package lesson191205;

import lesson191112.Fibonacci;

public class ThreadStateExample {

    public static void main(String[] args) throws InterruptedException {
        Mutex mutex = new Mutex();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long fib = Fibonacci.fib(20);
                System.out.println(fib);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                mutex.lock();


            }
        });

        Thread blockedThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mutex.unlock();
            }
        });

        System.out.println(thread.getState());

        thread.start();

        Thread.sleep(1000);

        blockedThread.start();

        while (thread.isAlive()) {
            Thread.sleep(500);
            System.out.println("thread state: " + thread.getState());
            System.out.println("blocked thread state: " + blockedThread.getState());
        }
    }
}

class Mutex {

    public synchronized void lock() {
//        Fibonacci.fib(1000);

        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (true) {
            System.out.println("Lock...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void unlock() {
        this.notify();

        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Unlock...");
        }
    }
}
