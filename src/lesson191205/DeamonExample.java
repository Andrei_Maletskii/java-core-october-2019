package lesson191205;

public class DeamonExample {


    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new ServiceThread(1));
        Thread thread2 = new Thread(new ServiceThread(2));
        Thread thread3 = new Thread(new ServiceThread(3));

        thread1.setDaemon(true);
        thread2.setDaemon(true);
        thread3.setDaemon(true);

        thread1.start();
        thread2.start();
        thread3.start();

//        thread1.setDaemon(false); can't set daemon or not after start


        for (int i = 0; i < 20; i++) {
            Thread.sleep(300);
        }

        System.out.println("Main completed");
    }
}

class ServiceThread implements Runnable {

    private int id;
    private int count = 0;

    public ServiceThread(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Service thread " + id + " is working..." + count);
        }
    }
}
