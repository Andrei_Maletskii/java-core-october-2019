package lesson191205;

public class ThreadExceptionHandling {

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread t, Throwable e) {
                        System.out.println(t.getName() + " threw " + e.getMessage());
                    }
                });

        Thread thread1 = new Thread(new ThrowException());
        thread1.setName("thread1");
        Thread thread2 = new Thread(new ThrowException());
        thread2.setName("thread2");
        thread2.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
//                throw new RuntimeException(e);
                System.out.println(t.getName() + " hooray");
            }
        });

        thread1.start();
        thread2.start();

    }
}

class ThrowException implements Runnable {

    @Override
    public void run() {
        throw new RuntimeException("Issues...");
    }
}
