package lesson191205;

public class MainDeath {

    public static void main(String[] args) {
        Thread main = Thread.currentThread();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Main state = " + main.getState());
                }
            }
        });

        thread.start();

        System.out.println("Main completed");
        throw new RuntimeException();
    }
}
