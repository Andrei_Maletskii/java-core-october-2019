package lesson191205;

public class ThreadMethods {

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("i = " + i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();

        System.out.println("thread id = " + thread.getId());
        System.out.println("main id = " + Thread.currentThread().getId());
    }
}
