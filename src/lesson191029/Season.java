package lesson191029;

import java.util.Arrays;

public enum Season {
//    public static final
    WINTER("white") {
        @Override
        public void greet() {
            System.out.println("Go away");
        }
    },
    SPRING("green"),
    SUMMER("green"),
    AUTUMN("yellow");

    private String color;

    private Season(String color) {
        this.color = color;
    }

    public void greet() {
        System.out.println("HELLO");
    }

    public static void main(String[] args) {
        Season winter1 = Season.WINTER;
        Season winter2 = Season.WINTER;

        System.out.println(winter1 == winter2);

//        Season season1 = Season.SUMMER;

        Season[] values = Season.values();

        Season winter = Season.valueOf("WINTER");

        WeekDay monday = Enum.valueOf(WeekDay.class, "MONDAY");
//        WeekDay monday1 = Enum.valueOf(WeekDay.class, "monday");


        WeekDay[] weekDays = WeekDay.values();
        System.out.println(Arrays.toString(weekDays));
        System.out.println("TUESDAY index " + WeekDay.TUESDAY.ordinal());


    }
}

enum WeekDay {
    MONDAY,
    TUESDAY,
    WEDNESDAY
}
