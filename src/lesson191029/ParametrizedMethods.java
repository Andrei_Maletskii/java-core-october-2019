package lesson191029;

public class ParametrizedMethods<T> {

//    private static T value1; ERROR
    private T value;

    public static <E extends Number> short utilMethod(E element) {
//        T value; ERROR
        return element.shortValue();
    }

    public <K extends String> int parametrizedInstanceMethod(K str) {
        return str.length();
    }

    @SuppressWarnings("all")
    public <N extends String> N returnDoubledStr(N str) {
        return (N) (str.concat(str));
    }

    @SuppressWarnings("unchecked")
    public static <M extends Number> M returnNumber(M number) {
        long l = number.longValue();
        Long aLong = Long.valueOf(l);
        return (M) aLong;
    }

    <M> int method() {
        return 0;
    }

    public static void main(String[] args) {
        ParametrizedMethods<Number> instance =
                new ParametrizedMethods<>();

        int length = instance.parametrizedInstanceMethod("hello");

        short integer = ParametrizedMethods.utilMethod(1);
        short aDouble = ParametrizedMethods.utilMethod(1.2);

        Double result = returnNumber(1.2);

        System.out.println(result);
    }
}
