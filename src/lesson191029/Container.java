package lesson191029;

public class Container<T> {
    private T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static void main(String[] args) {
        Container<Object> objectContainer = new Container<>();
        Container<A> aContainer = new Container<>();
        Container<B> bContainer = new Container<>();
        Container<C> cContainer = new Container<>();

        setContainerValue(objectContainer);
        setContainerValue(aContainer);
        setContainerValue(bContainer);
//        setContainerValue(cContainer); ERROR

//        printStoredValue(objectContainer); ERROR
//        printStoredValue(aContainer);
        printStoredValue(bContainer);
        printStoredValue(cContainer);


    }

    // PECS - Producer Extends Consumer Super

    // Consumer Super
    private static void setContainerValue(Container<? super B> container) {
//        container.setValue(new A()); ERROR
        container.setValue(new B());
        container.setValue(new C());

        Object value = container.getValue();
//        A value1 = container.getValue(); ERROR
//        B value2 = container.getValue();
//        C value3 = container.getValue();
    }

    // Producer Extends
    private static void printStoredValue(Container<? extends B> container) {
        Object value = container.getValue();
        System.out.println(value);

//        container.setValue(new Object()); ERROR
//        container.setValue(new A());
//        container.setValue(new B());
//        container.setValue(new C());
    }

}

class A {}
class B extends A {}
class C extends B {}
