package lesson191029;

public enum Singleton {
    INSTANCE(1);

    Singleton(int i) {
        this.i = i;
    }

    private int i;

    public void sayHello() {
        System.out.println("hello! " + i);
    }

    public static void main(String[] args) {
        Singleton.INSTANCE.sayHello();
    }
}
