package lesson191029;

public class ConstantsUse {
    public static final String WINTER = "WINTER";
    public static final String SUMMER = "SUMMER";

    public static void printGreeting(String season) {
        switch (season) {
            case "WINTER":
                System.out.println("Go away");
                break;
            case "SUMMER":
                System.out.println("HELLO!");
                break;
            default:
                throw new RuntimeException("Wrong season");
        }
    }

    public static void printGreeting(Season season) {
        switch (season) {
            case WINTER -> System.out.println("Go away");
            case SUMMER, AUTUMN, SPRING -> System.out.println("HELLO");
        }
    }

    public static void printGreetingStrategy(Season season) {
        season.greet();
    }

    public static void main(String[] args) {
        printGreetingStrategy(Season.WINTER);
        printGreetingStrategy(Season.SUMMER);
    }
}
