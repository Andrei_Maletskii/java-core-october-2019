package lesson191029;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface MyAnno {

    String defaultValue();

    int count();

    String[] values() default {"status"};

}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@interface MyDefaultAnno {

    int value() default 5;

    @MyCrazyAnno
    int count();
}

@interface MyCrazyAnno {

}


@MyAnno(defaultValue = "hello", count = 3, values = {"status", "hello", "hi"})
@MyDefaultAnno(count = 4)
class Z {

    @MyAnno(defaultValue = "hi", count = 4)
    private int i;
}
