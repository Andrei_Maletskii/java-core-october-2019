package lesson191202;

import java.util.*;

public class CollectionsExample {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(6, 4, 7, 4, 7, 10, 123, 68);

        // Stable, TimSort (MergeSort) N log N
        Collections.sort(integers);

        System.out.println(integers);

        List<Person> people = Arrays.asList(
                new Person("Petr", 30),
                new Person("Aleksei", 30)
        );

        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.age - o2.age;
            }
        });


        // DualPivotQuickSort for primitives, unstable, N log N
        Arrays.sort(new int[] {5,1,4,7,8,3});


    }
}

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
