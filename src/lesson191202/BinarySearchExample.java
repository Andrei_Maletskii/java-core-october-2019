package lesson191202;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BinarySearchExample {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(5, 8, 3, 6, 10, 22, 76, 0, 6);

        Collections.sort(list);

        System.out.println(list);
        System.out.println(Collections.binarySearch(list, 9));
    }
}
