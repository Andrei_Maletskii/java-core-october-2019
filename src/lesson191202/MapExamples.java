package lesson191202;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapExamples {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();

        System.out.println(map.put("key1", "value1"));
        System.out.println(map.put("key1", "value2"));

        System.out.println(map.get("key1"));

        map.containsKey("key1");
        map.containsValue("value2");


//        map.remove("key1");

        map.put("key2", "value2");

        Collection<String> values = map.values();
        Set<String> keySet = map.keySet();
        Set<Map.Entry<String, String>> entries = map.entrySet();

        System.out.println("map: " + map);
        System.out.println("values: " + values);
        System.out.println("keys: " + keySet);
        System.out.println("entries: " + entries);

        map.remove("key1");

        System.out.println("map: " + map);
        System.out.println("values: " + values);
        System.out.println("keys: " + keySet);
        System.out.println("entries: " + entries);

        for (Map.Entry<String, String> entry : entries) {
            entry.setValue("hello");
        }

        System.out.println(map);
    }
}
