package lesson191202;

import java.util.HashMap;

public class HashMapExample {

    public static void main(String[] args) {
        MyKey myKey = new MyKey("hello", 10);

        HashMap<MyKey, String> map = new HashMap<>(10);

        map.put(myKey, "myValue");
        System.out.println(map.get(myKey));

        myKey.setCount(Integer.MAX_VALUE);

        System.out.println(map.get(myKey));
        System.out.println(map);

        for (int i = 20; i < 40; i++) {
            map.put(new MyKey("key", i), "myValue" + i);
        }
        System.out.println(map.get(myKey));

    }
}

class MyKey {
    private String name;
    private Integer count;

    public MyKey(String name, Integer count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyKey myKey = (MyKey) o;

        if (name != null ? !name.equals(myKey.name) : myKey.name != null) return false;
        return count != null ? count.equals(myKey.count) : myKey.count == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }
}
