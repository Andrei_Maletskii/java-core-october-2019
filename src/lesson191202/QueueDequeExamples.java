package lesson191202;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class QueueDequeExamples {


    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        queue.offer("one");
        queue.offer("two");
        queue.offer("three");

        queue.poll();
        queue.remove();

        queue.peek();
        queue.element();

        Deque<String> deque = new LinkedList<>();
        deque.offerFirst("one");
        deque.offerLast("two");

        deque.pollFirst();
        deque.pollLast();

        deque.removeFirst();
        deque.removeLast();

        deque.push("stackElem1");
        deque.push("stackElem2");

        deque.pop();
        deque.pop();

    }
}
