package lesson191202;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ArrayDequeExamples {

    public static void main(String[] args) {
        PriorityQueue<String> pq = new PriorityQueue<>(2);

        pq.offer("one");
        pq.offer("two");
        pq.offer("three");

        while (!pq.isEmpty()) {
            System.out.println(pq.poll());
        }

        BlockingQueue<String> bq = new LinkedBlockingQueue<>(2);
        System.out.println(bq.offer("one"));
        System.out.println(bq.offer("two"));
        System.out.println(bq.offer("three"));
    }
}
