package lesson191105;

public class StringReplaceExample {

    public static void main(String[] args) {
        String replaceResult = "Hello".replace('l', 'g');
        String replaceResult1 = "Hello".replace("ll", "tt");

        System.out.println(replaceResult);
        System.out.println(replaceResult1);

        System.out.println("Hello".startsWith("He"));
        System.out.println("Hello".startsWith("el", 1));


        String substring = "Hello".substring(2);
        String substring1 = "Hello".substring(1, 3);

        System.out.println(substring);
        System.out.println(substring1);

        System.out.println("      Hello     \n\r".trim());
        System.out.println("-----");
    }
}
