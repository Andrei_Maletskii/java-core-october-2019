package lesson191105;

public class EnsureCapacityExample {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        sb.append("hello");

        sb.ensureCapacity(1000);
//        sb.setLength(100);

        String s = sb.toString();

        System.out.println(s + "!");
    }
}
