package lesson191105;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

public class StringCharsetsExample {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String str = "Hello, World";

        byte[] bytes = str.getBytes();

        byte[] cp1251s = str.getBytes("cp1251");

        byte[] defaultBytes = str.getBytes(Charset.defaultCharset());

        byte[] ibm1364s = str.getBytes(Charset.forName("IBM1364"));


        System.out.println(Arrays.toString(bytes));
        System.out.println(Arrays.toString(cp1251s));
        System.out.println(Arrays.toString(defaultBytes));
        System.out.println(Arrays.toString(ibm1364s));
    }



}
