package lesson191105;

import java.io.FileNotFoundException;
import java.util.Formatter;

public class FormatterExamples {

    public static void main(String[] args) throws FileNotFoundException {
        Formatter formatter = new Formatter("output.txt");

        formatter.format("%s : %d", "Progress", 55);
        formatter.format("%s : %d", "Progress", 56);
        formatter.format("%s : %d", "Progress", 57);

        formatter.flush();

        formatter.format("%s : %d", "Progress", 58);

//        formatter.close();
    }
}
