package lesson191105;

public class StringBuilderExamples {

    private static final int N = 1000000;

    public static void main(String[] args) {

        long startSlow = System.currentTimeMillis();
        String resultSlow = concatStringSlow("hello", N);
        long stopSlow = System.currentTimeMillis();

        System.out.println("Slow result = " + (stopSlow - startSlow));

        long startFast = System.currentTimeMillis();
        String resultFast = concatStringFast("hello", N);
        long stopFast = System.currentTimeMillis();

        System.out.println("Fast result = " + (stopFast - startFast));

//        System.out.println(resultSlow);
//        System.out.println(resultFast);


    }

    // DO NOT DO LIKE THAT
    private static String concatStringSlow(String example, int times) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < times; i++) {
            result.append(example);
        }

        return result.toString();
    }

    private static String concatStringFast(String example, int times) {
        StringBuilder sb = new StringBuilder(times * example.length());

        for (int i = 0; i < times; i++) {
            sb.append(example);
        }

        return sb.toString();
    }
}
