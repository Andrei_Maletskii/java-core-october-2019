package lesson191105;

import java.util.Arrays;
import java.util.Locale;

public class LocaleExamples {

    public static void main(String[] args) {
        Locale defaultLocale = Locale.getDefault();

        System.out.println(defaultLocale);

        Locale[] availableLocales = Locale.getAvailableLocales();

        System.out.println(Arrays.toString(availableLocales));


    }
}
