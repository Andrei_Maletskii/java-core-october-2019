package lesson191105;

public class ConcatVsConcatOperator {

    public static void main(String[] args) {
        String str = "abc";
        String empty = "";

        String result1 = str + "";
        String result2 = str.concat("");

        System.out.println(str == result1);
        System.out.println(str == result2);
    }
}
