package lesson191212;

import java.util.Random;
import java.util.function.Supplier;

public class SupplierExample {

    public static void main(String[] args) {
        Supplier<String> supplier = new Supplier<String>() {
            @Override
            public String get() {
                return "hello";
            }
        };

        Supplier<String> supplier1 = () -> {
            return "hello";
        };

        Supplier<String> supplier2 = () -> "hello";


//        print(supplier2);
        print(() -> getRandomString());
        print("hello");
    }

    private static String getRandomString() {
        Random random = new Random();

        return random.nextBoolean() ? random.nextBoolean() ? "hello" : "halo" : "hi";
    }

    private static void print(Supplier<String> supplier) {
        System.out.println(supplier.get());
    }

    private static void print(String str) {
        System.out.println(str);
    }
}
