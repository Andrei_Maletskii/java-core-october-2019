package lesson191212;

public class AnonymousExample {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        };
    }

    private static void process(Runnable runnable) {
        runnable.run();
    }
}
