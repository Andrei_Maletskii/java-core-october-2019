package lesson191212;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListSortExample {

    public static void main(String[] args) {
        List<String> list = Arrays.asList(
                "str",
                "str2",
                "str32",
                "str3254",
                "str326236"
        );

        Collections.sort(list);
        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });

        list.sort((str1, str2) -> {
            return str1.length() - str2.length();
        });
    }
}
