package lesson191212;

public class LambdaExample {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            System.out.println("hello");
        };

        process(runnable);
    }

    private static void process(Runnable runnable) {
        runnable.run();
    }
}
