package lesson191212;

import java.util.function.Predicate;

public class PredicateExamples {

    public static void main(String[] args) {
        Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        };

        Predicate<String> predicate1 = (String s) -> {
            System.out.println("hello");
            System.out.println("hello1");
            System.out.println("hello2");

            return s.isEmpty();
        };

        Predicate<String> predicate2 = (s) -> {
            return s.isEmpty();
        };

        Predicate<String> predicate3 = s -> {
            return s.isEmpty();
        };

        Predicate<String> predicate4 = s -> s.isEmpty();

        Predicate<String> predicate5 = String::isEmpty;


    }
}
