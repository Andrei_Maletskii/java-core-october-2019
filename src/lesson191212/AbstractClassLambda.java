package lesson191212;

public class AbstractClassLambda {

    public static void main(String[] args) {
//        methodA(i -> i);
        methodA(new A() {
            @Override
            public int method(int i) {
                return i + i;
            }
        });
        methodB(i -> i + i);
    }

    public static void methodA(A a) {
        System.out.println(a.method(1));
    }

    public static void methodB(B b) {
        System.out.println(b.method(1));
    }
}

//@FunctionalInterface
abstract class A implements B {
    public abstract int method(int i);
}

@FunctionalInterface
interface B {
    int method(int i);
}
