package lesson191212;

import java.awt.*;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Flow;
import java.util.function.*;

public class MethodReferenceExample {

    public static void main(String[] args) {
        // ([args]) -> ClassName.staticMethod([args]) [] - optional

        // () -> ClassName.staticMethod()
        Supplier<Thread> currentThread1 = () -> Thread.currentThread();
        Supplier<Thread> currentThread2 = Thread::currentThread;

        // (args) -> ClassName.staticMethod(args)
        Function<String, List<String>> stringListFunction = (String s) -> List.of(s);
        Function<String, List<String>> runnable = List::of;

        BiFunction<String, String, List<String>> biFunction =
                (str1, str2) -> List.of(str1, str2);
        BiFunction<String, String, List<String>> biFunction1 = List::of;
        Function<String[], List<String>> biFunction2 = List::of;


        // (arg, [rest...])  -> arg.instanceMethod([rest])
        // (comparator, a, b)  -> comparator.compare(a,b)
        // ClassName::instanceMethod

        BiPredicate<Supplier<String>, String> biPredicate =
                (supplier, prefix) -> supplier.get().startsWith(prefix);



        ArrayList<String> arrayList = new ArrayList<>();

        BiFunction<ArrayList<String>, String, Boolean> ref1 = (list, str) -> list.add(str);
        BiConsumer<ArrayList<String>, String> ref0 = ArrayList::add;
//        ref0.accept();
        BiFunction<ArrayList<String>, String, Boolean> ref2 = ArrayList::add;
//        ref2.apply()

        Comparor<String> comparor = (comparator, a, b) -> comparator.compare(a, b);
        Comparor<String> comparor10 = Comparator::compare;

        // (args)  ->  expr.instanceMethod(args)
        // expr::instanceMethod

        Predicate<String> predicate = (prefix) -> getString().startsWith(prefix);
        Predicate<String> predicate1 = getString()::startsWith;

        Consumer<String> consumer = System.out::println;
        Consumer<Object> consumer1 = (obj) -> {
            PrintStream out = System.out;

            out.println(obj);
        };

        consumeAnything("hello", System.out::println);


        // (args) -> new ClassName(args)
        // ClassName::new

        Function<byte[], String> function = byteArray -> new String(byteArray);
        Function<byte[], String> function1 = String::new;

        Supplier<ArrayList> runnable1 = ArrayList::new;



        Comparor<String> comparor1 = Comparator::compare;

        int result = comparor1.compareWithComparator(
                (str1, str2) -> str1.length() - str2.length(),
                "hello",
                "hello2"
        );
    }

    public static void consumeAnything(String str, Consumer<String> consumer) {
        consumer.accept(str);
    }

    public static String getString() {
        return "hello";
    }
}

@FunctionalInterface
interface Comparor<T> {
    int compareWithComparator(Comparator<T> comparator, T a, T b);

}
