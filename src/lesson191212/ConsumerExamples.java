package lesson191212;

import java.util.function.Consumer;
import java.util.function.Function;

public class ConsumerExamples {

    public static void main(String[] args) {
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };

        Consumer<String> consumer1 = (String s) -> {
            System.out.println(s);
        };

        Consumer<String> consumer2 = (s) -> {
                System.out.println(s);
        };

        Consumer<String> consumer3 = s -> {
            System.out.println(s);
        };

        // args -> expr.instanceMethod(args)
        // expr::instanceMethod
        Consumer<String> consumer4 = s -> System.out.println(s);
        Consumer<String> consumer5 = System.out::println;



        System.out.println(consumer4);
    }
}
