package lesson191212;

import java.util.Comparator;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class EffectivelyFinalExample {

}

class X {
    int instanceInt;
    static int staticInt;

    public void method(int arg) {
        final int localInt = 10;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // int localInt = localInt
                // int arg = arg

                System.out.println(X.this.instanceInt);
                System.out.println(X.staticInt);
//                System.out.println(arg);
                System.out.println(localInt);
            }
        };
        Runnable lambda = () -> {
            System.out.println(this.instanceInt);
            System.out.println(X.staticInt);
//            System.out.println(arg);
            System.out.println(localInt);
        };
        arg++;
//        localInt++;
        instanceInt++;
        staticInt++;

        runnable.run();
        lambda.run();
    }

    public void method2(Container container) {
        Runnable runnable = () -> {
            System.out.println(container.i1);
            System.out.println(container.i2);
        };
        container.i1++;
        container.i2++;
//        container = new Container(1, 2);

        Callable<String> callable = new Callable<>() {
            @Override
            public String call() throws Exception {
                return "str";
            }
        };

        Callable<String> stringCallable = () -> "str";

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        };

        Comparator<String> lambdaComparator = (o1, o2) -> o1.length() - o2.length();
        BiFunction<String, String, Integer> function = (o1, o2) -> o1.length() - o2.length();

        acceptComparator(lambdaComparator);
        acceptComparator1(lambdaComparator);
        acceptBiFunction(function);

        acceptComparator((o1, o2) -> o1.length() - o2.length());
        acceptBiFunction((o1, o2) -> o1.length() - o2.length());
    }

    private void acceptComparator(Comparator<String> comparator) {
        comparator.compare("hello", "hello2");
    }

    private void acceptComparator1(Comparator<String> comparator) {
        comparator.compare("hello", "hello2");
    }

    private void acceptBiFunction(BiFunction<String, String, Integer> comparator) {
        comparator.apply("hello", "hello2");
    }
}

class Container {
    int i1;
    int i2;

    public Container(int i1, int i2) {
        this.i1 = i1;
        this.i2 = i2;
    }
}
