package lesson191107;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupExample {

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(B(C(D)))(A)");

        Matcher matcher = pattern.matcher("BCDA");

        System.out.println(matcher.groupCount());
        boolean matches = matcher.matches();

        System.out.println(matcher.group(0));
        System.out.println(matcher.group(1));
        System.out.println(matcher.group(2));
        System.out.println(matcher.group(3));
        System.out.println(matcher.group(4));
    }
}
