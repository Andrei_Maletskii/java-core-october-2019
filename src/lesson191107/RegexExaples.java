package lesson191107;

public class RegexExaples {

    public static void main(String[] args) {
        System.out.println("a".matches("[abc]"));
        System.out.println("j".matches("[^abc]"));
        System.out.println("c".matches("[abc]"));
        System.out.println("Y".matches("[a-gH-Z]"));

        System.out.println("cbca".matches("^[a-c]*"));
        System.out.println("cbca".matches("[helo]|(h)"));
        System.out.println("cbca".matches("hell?o"));
        System.out.println("cbca".matches("hel*o"));
        System.out.println("cbca".matches("hel+o"));
        System.out.println("cbca".matches("hel{3}o"));
        System.out.println("cbca".matches("hel{3,}o"));
        System.out.println("cbca".matches("hel{3,5}o"));
        System.out.println("cbca".matches("hel{2,4}o"));
        System.out.println("cbca".matches(".*"));
        System.out.println("cbca".matches(" +"));

        "hello world".contains("hello");

    }
}
