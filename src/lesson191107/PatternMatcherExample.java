package lesson191107;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherExample {
    private static final Pattern PATTERN = Pattern.compile("(abc)+");

    public static void main(String[] args) {
        Matcher matcher = PATTERN.matcher("abcabcabc");

        boolean matches = matcher.matches();

        System.out.println(matcher.end());

        matcher.reset("abc");

        String result = matcher.replaceAll("xyz");

        System.out.println(result);

//        boolean matches = Pattern.matches("(abc)+", "abcabcabc");


    }
}
