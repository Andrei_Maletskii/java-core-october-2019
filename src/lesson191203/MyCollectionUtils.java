package lesson191203;

import java.io.Serializable;
import java.util.*;

public class MyCollectionUtils {

    public static <T extends Iterable<? super T> & Comparable<? super T>> T min(Collection<? extends T> coll) {
        Iterator<? extends T> i = coll.iterator();
        T candidate = i.next();

        candidate.iterator();
        candidate.compareTo(null);

        return candidate;
    }

    public static void main(String[] args) {
        Set<String> objects = Collections.newSetFromMap(new HashMap<>());

        System.out.println(objects);

        Queue<String> stack = Collections.asLifoQueue(new LinkedList<>());

        stack.offer("one");
        stack.offer("two");
        stack.offer("three");

        System.out.println(stack.poll());

    }
}
