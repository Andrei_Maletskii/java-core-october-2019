package lesson191203;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CollectionsShuffleAndRandom {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);

        Random random = new Random(100);

        Collections.shuffle(list, random);

        System.out.println(list);

        Random myRandom = new Random();

        int i = myRandom.nextInt(1000) - 500;
        System.out.println(i);
    }
}
