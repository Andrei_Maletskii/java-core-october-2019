package lesson191203.thread;

public class JoinExample {

    public static void main(String[] args) {
        System.out.println("Main started");
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Started...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    //NOP
                }
                System.out.println("Finished...");
            }
        });

        myThread.start();

        try {
            myThread.join(); // main waits for myThread completion
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Other myThread finished, we were waiting...");
    }
}
