package lesson191203.thread;

public class TalkAndWalk {
    public static void main(String[] args) throws InterruptedException {
        TalkingThread talkingThread = new TalkingThread();
        talkingThread.start();

        Thread thread = new Thread(new WalkingRunnable());
        thread.start();

        Thread.sleep(1500);
        System.out.println("Suspend Walking");
        thread.suspend();
        Thread.sleep(1500);
        System.out.println("Resume walking");
        thread.resume();
        Thread.sleep(1500);
        System.out.println("Stop walking");
        thread.stop();


    }
}
