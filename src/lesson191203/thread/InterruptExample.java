package lesson191203.thread;


import lesson191112.Fibonacci;

public class InterruptExample {

    private static boolean flag = true;

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (!Thread.interrupted()) {
                    System.out.println(Fibonacci.fib(i++));
                }

                System.out.println(Thread.interrupted());
            }
        });

        thread.start();

        Thread.sleep(1000);
        thread.interrupt();

//        thread.isInterrupted()
    }
}
