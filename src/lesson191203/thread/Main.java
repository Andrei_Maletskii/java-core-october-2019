package lesson191203.thread;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(10000);

        System.out.println(Thread.currentThread().getName());
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
        });

        thread.start();
        Thread.sleep(10000);


    }
}
