package lesson191203.thread;

public class StartOrRun {

    public static void main(String[] args) {
        new MyThread().run();
        new MyThread().start();
    }
}

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}
