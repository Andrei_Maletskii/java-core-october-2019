package lesson191203.thread;

public class PrioritizedTalkAndWalk {
    public static void main(String[] args) throws InterruptedException {
        TalkingThread talkingThread = new TalkingThread();
        Thread walkingThread = new Thread(new WalkingRunnable());
        talkingThread.setName("TalkingThread");
        talkingThread.setPriority(Thread.MAX_PRIORITY);
        walkingThread.setName("WalkingThread");
        walkingThread.setPriority(Thread.MIN_PRIORITY);

        talkingThread.start();
        walkingThread.start();


        Thread.yield();

        new Object().wait();



    }
}
