package lesson191203;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CollectionIndexOfSublist {


    public static void main(String[] args) {
        List<Integer> bigOne = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 4, 5, 6);
        List<Integer> smallOne = Arrays.asList(4, 5, 6);

        System.out.println(Collections.indexOfSubList(bigOne, smallOne));
        System.out.println(Collections.lastIndexOfSubList(bigOne, smallOne));

        Collections.swap(smallOne, 0, 2);

        System.out.println(smallOne);

//        Collections.checkedCollection();
//        Collections.unmodifiableCollection();
//        Collections.synchronizedCollection();
//        Collections.singleton()
        System.out.println(Collections.disjoint(bigOne, smallOne));
    }
}
