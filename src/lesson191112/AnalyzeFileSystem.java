package lesson191112;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class AnalyzeFileSystem {
    public static void main(String[] args) throws IOException {
        File file = new File(".");

        printAllNestedFilesRecursively(file);
        printAllNestedFiles(file);
    }

    public static void printAllNestedFilesRecursively(File root) throws IOException {
        File[] files = root.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                printAllNestedFilesRecursively(file);
            }
        }
        System.out.println(root.getCanonicalPath());
    }

    public static void printAllNestedFiles(File root) throws IOException {
        Queue<File> stack = new LinkedList<>();

        stack.offer(root);

        while (!stack.isEmpty()) {
            File file = stack.poll();

            System.out.println(file.getCanonicalPath());

            if (!file.isDirectory()) {
                continue;
            }

            File[] filesToProcess = file.listFiles();

            for (File fileToProcess : filesToProcess) {
                stack.offer(fileToProcess);
            }
        }

    }
}
