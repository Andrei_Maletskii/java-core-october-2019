package lesson191112;

import java.math.BigInteger;

public class Fibonacci {

    // 0, 1, 1, 2, 3, 5, 8, 13, 21,
    // 34, 55, 89, 144, 233, 377, 610,
    // 987, 1597, 2584, 4181, 6765, 10946,
    // 17711, 28657, 46368, 75025, 121393, 196418, 317811...

    public static long fib(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        if (n <= 1) {
            return n;
        }

        return fib(n - 2) + fib(n - 1);
    }

    static BigInteger[] cache = new BigInteger[1000];

    public static BigInteger fibFast(int n) {
        if (n <= 1) {
            return BigInteger.valueOf(n);
        }

        if (cache[n] != null) {
            return cache[n];
        } else {
            cache[n] = fibFast(n - 1).add(fibFast(n - 2));
            return cache[n];
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            estimateFib(i);
        }
    }

    private static void estimateFib(int n) {
        long start = System.currentTimeMillis();
        BigInteger result = fibFast(n);
        long stop = System.currentTimeMillis();
        System.out.println("Result: " + result);
        System.out.println("Estimated: " + (stop - start));
    }
}
