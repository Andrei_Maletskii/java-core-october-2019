package lesson191112;

import java.util.Arrays;
import java.util.List;

public class RecursionExample {

    public static void main(String[] args) {
        List<Integer> integers = List.of(1, 2, 3, 4, 5);

//        for (Integer integer : integers) {
//            System.out.println(integer);
//        }

        soutI(1, 5);
    }

    public static void soutI(int i, int max) {
        if (i > max) {
            return;
        }
        soutI(i + 1, max);
        System.out.println(i);
    }
}
