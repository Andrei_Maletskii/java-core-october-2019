package lesson191119;

public class CatchHierarchy {

    public static void main(String[] args) {
        int i = 0;

        try {
            throwSomeException(i);
        } catch (MyChild1Exception | MyChild2Exception e) {
            e.printStackTrace();
        } catch (MyBaseException e) {
            System.out.println("Base exception");
        } catch (Exception e) {

        } /*catch (Throwable e) {

        }*/
    }

    private static void throwSomeException(int i) throws Exception {
        if (i == 1) {
            throw new MyChild1Exception();
        }

        if (i == 2) {
            throw new MyChild2Exception();
        }

        throw new MyBaseException();
    }
}

class MyBaseException extends Exception {

}

class MyChild1Exception extends MyBaseException {

}

class MyChild2Exception extends MyBaseException {

}
