package lesson191119;

public class NestedTry {

    public static void main(String[] args) {
        try {
            try {
                throw new NullPointerException();
            } catch (NullPointerException e) {
//            } catch (ArithmeticException e) {
               e.printStackTrace();
//               throw e;
            }
            System.out.println("Will be executed in current example");
//            System.out.println("Won't be executed in current example");
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }

    private static void getException() {
        throw  new RuntimeException();
    }
}
