package lesson191119;

import java.io.*;

public class TryWithResources {

    public static void main(String[] args) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("hello"))
        ) {
            oos.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        oos.reset(); ERROR


        ObjectOutputStream oos1 = null;
        try {
            oos1 = new ObjectOutputStream(new FileOutputStream("hello"));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                oos1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (FileInputStream fis = new FileInputStream("hello");
                ObjectInputStream ois = new ObjectInputStream(fis)) {

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
