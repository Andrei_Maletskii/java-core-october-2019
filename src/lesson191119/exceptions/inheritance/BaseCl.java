package lesson191119.exceptions.inheritance;

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.FileSystemException;

public class BaseCl {
    public BaseCl() throws IOException, ArithmeticException {
    }

    public void methodBase() throws IOException {

    }

    public void methodIO() throws EOFException, FileSystemException {

    }

    public static void methodA() throws IOException {
    }
}

