package lesson191119.exceptions.inheritance;

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.FileSystemException;

public class DerivativeCl2 extends BaseCl {
    // ошибок компиляции нет
    public DerivativeCl2() throws Exception {
        super();
    }

    // compile error
    @Override
    public void methodIO() throws EOFException, FileSystemException {

    }

    //    @Override
//    public void methodBase() throws Exception {
//
//    }

//    compile error
//    public static void methodA() throws Exception {
//    }
}

