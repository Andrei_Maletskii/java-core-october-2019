package lesson191119;

public class BadExceptionPractice {
    static int[] ints = new int[] {1, 2, 3, 4, 5};

    public static void main(String[] args) {
        System.out.println(getElement(2));
        System.out.println(getElement(10));
    }


    private static int getElement(int index) {
        try {
            return ints[index];
        } catch (ArrayIndexOutOfBoundsException ex) {
            return -1;
        }
    }

    private static int getElementGood(int index) {
        if (index >= ints.length) {
            return -1;
        }

        return ints[index];
    }


}
