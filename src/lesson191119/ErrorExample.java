package lesson191119;

public class ErrorExample {

    public static void main(String[] args) {
        try {
            rec();
        } catch (Error e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            System.out.println("Error");
        }
    }

    private static void rec() {
        rec();
    }
}

class MyError extends Error {

}


