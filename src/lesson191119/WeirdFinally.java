package lesson191119;

public class WeirdFinally {

    public static void main(String[] args) {
        // 1
        System.out.println(returnInt());

        // 2
        try {
            throwEx();
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }

        // 3
        shutDown();
    }

    private static int throwEx() {
        try {
            throw new RuntimeException();
        } catch (RuntimeException e) {
            throw new MyRuntimeException("myRuntimeException");
        } finally {
            throw new Error("My own error");
        }
    }

    private static int returnInt() {
        try {
            return 1;
        } finally {
            return 2;
        }
    }

    private static void shutDown() {
        try {
            System.exit(-1);
        } finally {
            System.out.println("not executed after system.exit");
        }
    }
}
