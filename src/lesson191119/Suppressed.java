package lesson191119;

public class Suppressed {

    public static void main(String[] args) {
        RuntimeException re = new RuntimeException();

        re.addSuppressed(new Exception());

        Throwable[] suppressed = re.getSuppressed();
    }
}
