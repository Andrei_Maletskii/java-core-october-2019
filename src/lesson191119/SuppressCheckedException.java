package lesson191119;

public class SuppressCheckedException {
    public static void main(String[] args) {
        ThrowingExceptionObject teo = new ThrowingExceptionObject();
        try {
            teo.method();
        } catch (Exception e) {
            e.printStackTrace();
        }

        teo.sneakyMethod();

    }

}

class ThrowingExceptionObject {

    public void method() throws Exception {

    }

    public void sneakyMethod() {
        try {
            method();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

