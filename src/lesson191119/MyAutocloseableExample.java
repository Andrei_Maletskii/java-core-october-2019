package lesson191119;

import java.io.Closeable;
import java.io.IOException;

public class MyAutocloseableExample {
    public static void main(String[] args) {
        try (MyCloseable c = new MyCloseable()) {
            System.out.println(c);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            System.out.println("finally");
        }
    }

}

class MyCloseable implements Closeable {

    @Override
    public void close() throws IOException {
        System.out.println("Try to close...");
        throw new IOException("My IO exception");
    }
}
