package lesson191119;

public class RuntimeThrows {

    public static void main(String[] args) {
        method();
    }

    private static void method() throws MySuperRuntimeException {
        throw new MySuperRuntimeException();
    }

    private static int divideFiveByInt(int i) {
        if (i == 0) {
            throw new IllegalArgumentException("i can't be zero");
        }

        return 5 / i;
    }
}

class MySuperRuntimeException extends RuntimeException {

}
