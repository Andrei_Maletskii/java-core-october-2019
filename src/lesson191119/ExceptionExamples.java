package lesson191119;

public class ExceptionExamples {

    public static void main(String[] args) {
        try {
            throwRuntimeException();
        } catch (RuntimeException ex) {

        }
//        throwRuntimeException();

        try {
            throwException();
        } catch (Exception e) {
            throw new MyRuntimeException("hello", e);
        }
    }

    private static void throwException() throws Exception {
        throw new Exception("exception");
    }

    private static void throwRuntimeException() {
        throw new RuntimeException("runtime ex");
    }

    private static void throwMyRuntimeException() {
        throw new MyRuntimeException("runtime ex");
    }
}

class MyRuntimeException extends RuntimeException {
    public MyRuntimeException(String message) {
        super(message);
    }

    public MyRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyRuntimeException(int i) {
        super();
    }

    public static void method() {

    }
}
