package lesson191119;

import java.util.Iterator;

public class UnsupportedMethodExample {


}

class MyIterator implements Iterator<Integer> {

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Integer next() {
        throw new UnsupportedOperationException();
    }
}

class MyNewIterator extends MyIterator {
    @Override
    public Integer next() {
        return 1;
    }
}
