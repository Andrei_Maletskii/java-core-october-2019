package lesson191119;

import java.io.FileInputStream;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {
        Stream.of(1, 2, 3, 4, 5).map(i -> {
            try {
                return map(i);
            } catch (Exception e) {
                e.printStackTrace();
                return "default";
            }
        });

        Stream.of(1, 2, 3, 4, 5).map(StreamExample::map);
    }

    private static String map(Integer i) {
        if (i > 4) {
            throw new RuntimeException();
        }

        return i.toString();
    }
}
