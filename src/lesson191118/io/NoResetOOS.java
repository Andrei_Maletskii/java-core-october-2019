package lesson191118.io;

import lesson191118.io.geometry.Line;
import lesson191118.io.geometry.Point;

import java.io.*;

public class NoResetOOS {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Line line = new Line(
                new Point(0, 0),
                new Point(1, 1),
                "line"
        );

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("noReset"));

        oos.writeObject(line);
        oos.writeObject(line);

        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("noReset"));

        Object object1 = ois.readObject();
        Object object2 = ois.readObject();

        System.out.println(object1 == object2); // true

        ois.close();
    }
}
