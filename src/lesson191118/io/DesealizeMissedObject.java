package lesson191118.io;

import java.io.*;

public class DesealizeMissedObject {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("missed"));

        Object object = ois.readObject();

        ois.close();

        System.out.println(object.getClass());
        System.out.println(object.toString());
    }
}
