package lesson191118.io;

import java.io.*;

public class InheritanceSerialization {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        B b1 = new B();
        oos.writeObject(b1);

        byte[] serialized = baos.toByteArray();

        oos.close();


        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(serialized));

        System.out.println("Deserialization");

        B b2 = (B) ois.readObject();

        System.out.println(b1 == b2); // false

        ois.close();
    }
}

class A {
    public A(int i) { // ONLY DEFAULT
        System.out.println("Constructing A");
    }
}

class B extends A implements Serializable {

    public B() {
        super(1);
        System.out.println("Constructing B");
    }
}
