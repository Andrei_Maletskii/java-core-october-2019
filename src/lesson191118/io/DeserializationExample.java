package lesson191118.io;

import lesson191118.io.geometry.Line;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationExample {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream("serializedLine1")
        );

        Object object = ois.readObject();

        if (object instanceof Line) {
            Line line = (Line) object;

            System.out.println(line.getP1().getX());
            System.out.println(line.getP1().getY());

            System.out.println(line.getP2().getX());
            System.out.println(line.getP2().getY());

            System.out.println(line.getName());
        }

        ois.close();
    }
}
