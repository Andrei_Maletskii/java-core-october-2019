package lesson191118.io.versioning.serialization;

import java.io.Serializable;

public class OldVersion implements Serializable {
    private static final long serialVersionUID = -4124434834536155236L;

    int i;
    int y;

    public OldVersion(int i, int y) {
        this.i = i;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OldVersion that = (OldVersion) o;

        if (i != that.i) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = i;
        result = 31 * result + y;
        return result;
    }
}
