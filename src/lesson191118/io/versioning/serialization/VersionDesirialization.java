package lesson191118.io.versioning.serialization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class VersionDesirialization {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois =
                new ObjectInputStream(new FileInputStream("serializedOldVersion"));
        OldVersion oldVersion = (OldVersion) ois.readObject();

        System.out.println(oldVersion.i);
        System.out.println(oldVersion.y);

        ois.close();
    }
}
