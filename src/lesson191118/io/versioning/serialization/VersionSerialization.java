package lesson191118.io.versioning.serialization;

import java.io.*;

public class VersionSerialization {

    public static void main(String[] args) throws IOException {
        ObjectOutputStream oos =
                new ObjectOutputStream(new FileOutputStream("serializedOldVersion"));

        OldVersion oldVersion = new OldVersion(45, 77);

        oos.writeObject(oldVersion);

        oos.close();
    }
}
