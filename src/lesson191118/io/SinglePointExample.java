package lesson191118.io;

import lesson191118.io.geometry.Line;
import lesson191118.io.geometry.Point;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SinglePointExample {

    public static void main(String[] args) throws IOException {
        Point p1 = new Point(0, 0);
        Line line = new Line(
                p1,
                p1,
                "MyLine"
        );

        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("serializedLine2")
        );

        oos.writeObject(line);

        oos.close();





    }
}
