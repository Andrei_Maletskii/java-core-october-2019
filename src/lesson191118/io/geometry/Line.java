package lesson191118.io.geometry;

import java.io.Serializable;

public class Line implements Serializable {

    static String str = getStr();

    private static String getStr() {
        System.out.println("hello generating");
        return "hello";
    }

    static {
        System.out.println("static init in Line class");
    }

    private Point p1;
    private Point p2;
    private transient String name = "defaultName";

    public Line(Point p1, Point p2, String name) {
        System.out.println("Constructing line...");

        this.p1 = p1;
        this.p2 = p2;
        this.name = name;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }

    public String getName() {
        return name;
    }
}
