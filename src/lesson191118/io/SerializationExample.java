package lesson191118.io;

import lesson191118.io.geometry.Line;
import lesson191118.io.geometry.Point;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationExample {

    public static void main(String[] args) throws IOException {
        Line line = new Line(
                new Point(0, 0),
                new Point(5, 5),
                "MyLine"
        );

        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("serializedLine1")
        );

        oos.writeObject(line);

        oos.close();





    }
}
