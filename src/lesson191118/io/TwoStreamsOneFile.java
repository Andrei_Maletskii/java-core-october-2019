package lesson191118.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TwoStreamsOneFile {

    public static void main(String[] args) throws IOException {
        FileInputStream fis1 = new FileInputStream("hello12345");
        FileInputStream fis2 = new FileInputStream("hello12345");



        fis1.close();
        fis2.close();
    }
}
