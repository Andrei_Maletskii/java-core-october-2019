package lesson191118.exceptions;

import com.sun.jdi.request.DuplicateRequestException;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class TryCatchFinallyExample {

    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream =
                new DataInputStream(new ByteArrayInputStream(new byte[] {0, 0, 0, 0}));

        try {
            int i = dataInputStream.readInt();

            System.out.println(i);

            System.out.println(5 / i);

        } catch (IOException | DuplicateRequestException | ArithmeticException e) {
            e.printStackTrace();
        } finally {
            dataInputStream.close();
        }

        try {

        } catch (Exception e) {

        }

        try {

        } catch (RuntimeException e) {

        } finally {

        }

        try {

        } finally {

        }

        try (FileInputStream fis = new FileInputStream("missed")) {

        }
    }
}
