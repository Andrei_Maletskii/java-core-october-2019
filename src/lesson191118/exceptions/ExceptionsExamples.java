package lesson191118.exceptions;

public class ExceptionsExamples {

    public static void main(String[] args) {
        try {
            exceptionalMethod();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            System.out.println("hello");
        }
        System.out.println("Exit");
    }

    private static void exceptionalMethod() {
        throw new RuntimeException();
    }
}
