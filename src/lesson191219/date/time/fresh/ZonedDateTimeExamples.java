package lesson191219.date.time.fresh;

import java.time.*;
import java.util.Set;

public class ZonedDateTimeExamples {

    public static void main(String[] args) {
        ZonedDateTime now = ZonedDateTime.now();

        ZoneOffset utc = ZoneOffset.UTC;

        ZonedDateTime now1 = ZonedDateTime.now(ZoneId.systemDefault());

        ZonedDateTime now2 = ZonedDateTime.of(
                LocalDate.now(),
                LocalTime.now(),
                ZoneId.systemDefault()
        );

        ZonedDateTime now3 = ZonedDateTime.of(
                LocalDateTime.of(
                        LocalDate.now(),
                        LocalTime.now()
                ),
                ZoneId.systemDefault()
        );

        System.out.println(now3);

        ZoneId zoneId = ZoneId.systemDefault();




        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();

//        System.out.println(availableZoneIds);


        ZoneId zone = now3.getZone();

        LocalDate localDate = now3.toLocalDate();
        LocalTime localTime = now3.toLocalTime();
        LocalDateTime localDateTime = now3.toLocalDateTime();

        ZoneId cuiaba = ZoneId.of("America/Cuiaba");

        ZonedDateTime cuiabaTime = ZonedDateTime.of(
                LocalDateTime.now(),
                cuiaba
        );

        System.out.println(cuiabaTime);
        ZonedDateTime now4 = ZonedDateTime.now(cuiaba);

        System.out.println(now4);

        ZonedDateTime cuiabaTimeNow = now.withZoneSameLocal(cuiaba);
        System.out.println(cuiabaTimeNow);

        ZonedDateTime cuiabaNewFinal = now.withZoneSameInstant(cuiaba);

        System.out.println(cuiabaNewFinal);
    }
}
