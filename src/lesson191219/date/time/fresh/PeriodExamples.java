package lesson191219.date.time.fresh;

import java.time.LocalDate;
import java.time.Period;

public class PeriodExamples {

    public static void main(String[] args) {
        Period threeDays = Period.ofDays(3);

        System.out.println(threeDays);

        Period days43 = threeDays.plusMonths(40);

        System.out.println(days43);

        Period normalized = days43.normalized();

        System.out.println(normalized);

        Period period1 = Period.of(1, 3, 15);

        Period newWeeksDays = Period.ofWeeks(3);

        Period twoDays = newWeeksDays.ofDays(2);

        Period period = Period.between(
                LocalDate.now().plusDays(-2),
                LocalDate.now().plusDays(3)
        );

        System.out.println(period);

        Period negated = period.negated();

        boolean isNegative = period.isNegative();

        System.out.println(negated);
    }
}
