package lesson191219.date.time.fresh;

import java.time.LocalTime;

public class LocalTimeExample {

    public static void main(String[] args) {
        LocalTime now = LocalTime.now();
        System.out.println(now);

        LocalTime now2 = LocalTime.of(19, 50, 30, 3);


        LocalTime after3Hours = now2.plusHours(3);
    }
}
