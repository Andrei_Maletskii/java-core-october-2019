package lesson191219.date.time.fresh;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class LocalDateTimeExample {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();

        System.out.println(now);

        LocalDateTime now2 = LocalDateTime.of(2019, Month.DECEMBER, 19, 19, 55, 55, 10);

        LocalDate nowDate = LocalDate.now();
        LocalTime nowTime = LocalTime.now();
        LocalDateTime now3 = LocalDateTime.of(nowDate, nowTime);

        LocalDateTime now4 = LocalDateTime.now();

        LocalDateTime after3Years = now4.plusYears(3);

        LocalDateTime oneNanoBefore = after3Years.minusNanos(1);

        LocalDate localDate = oneNanoBefore.toLocalDate();
        LocalTime localTime = oneNanoBefore.toLocalTime();
    }
}
