package lesson191219.date.time.fresh;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Collections;

public class DateTimeFormatterExamples {

    public static void main(String[] args) {
//        DateTimeFormatter formatter1 = new DateTimeFormatter();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
        DateTimeFormatter formatter1 = DateTimeFormatter.RFC_1123_DATE_TIME;

        String isoNowFormat = formatter.format(LocalDateTime.now());
//        String rfcFormatted = formatter1.format(LocalDateTime.now());
        String rfcFormatted = formatter1.format(ZonedDateTime.now());

        System.out.println(isoNowFormat);
        System.out.println(rfcFormatted);

        DateTimeFormatter myFormatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");

        String format = myFormatter.format(LocalDate.now());

        System.out.println(format);

        TemporalAccessor parse = myFormatter.parse("2019-Dec-24");

        System.out.println(parse);

        LocalDate parse1 = LocalDate.parse("2019-Dec-25");



    }
}
