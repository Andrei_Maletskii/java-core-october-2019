package lesson191219.date.time.fresh;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DurationExamples {

    public static void main(String[] args) {
        Duration estimated = Duration.between(
                Instant.now().plusSeconds(3),
                Instant.now().minusSeconds(3)
        );

        System.out.println(estimated);

        Duration twoMinutes = Duration.of(2, ChronoUnit.MINUTES);
//        Duration threeEras = Duration.of(3, ChronoUnit.ERAS);

        System.out.println(twoMinutes);
//        System.out.println(threeEras);


        Duration minutes = twoMinutes.plusMinutes(60);

        System.out.println(minutes);

        Duration sixMinutes = Duration.between(
                LocalTime.now().plusMinutes(3),
                LocalTime.now().plusMinutes(-3)
        );

        System.out.println(sixMinutes);

        boolean isNegative = sixMinutes.isNegative();


    }
}
