package lesson191219.date.time.fresh;

import java.time.*;
import java.time.temporal.ChronoField;
import java.util.Date;

public class LocalDateExamples {

    public static void main(String[] args) {
//        LocalDate yesterday = new LocalDate(2019, 12, 18);
        LocalDate yesterday = LocalDate.of(2019, Month.DECEMBER, 18);
        LocalDate start2020 = LocalDate.of(2020, 1, 1);

        LocalDate today = LocalDate.now();
        LocalDate today2 = LocalDate.now(Clock.systemDefaultZone());

        LocalDate today3 = LocalDate.ofEpochDay(0);

        System.out.println(today3);

        LocalDate today4 = LocalDate.from(LocalDateTime.of(2019, Month.DECEMBER, 19, 19, 41, 0));

        System.out.println(today4);

        LocalDate now = LocalDate.now();

        LocalDate after3Years = now.plusYears(3);

        LocalDate threeDaysBefore = now.minusDays(3);

//        now.isAfter()
        boolean result = now.isBefore(LocalDate.of(2019, Month.DECEMBER, 20));

        System.out.println(result);

        int i = now.get(ChronoField.DAY_OF_WEEK);
        DayOfWeek dayOfWeek = now.getDayOfWeek();



    }
}
