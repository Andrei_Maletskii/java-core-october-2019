package lesson191219.date.time.fresh;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;

public class InstantExamples {

    public static void main(String[] args) {
        Instant now = Instant.now();

        Instant now2 = Instant.ofEpochMilli(new Date().getTime());

        System.out.println(now);
        System.out.println(now2);

        Instant plusSecond = now.minusSeconds(-1);
        Instant minusSecond = now.plusSeconds(-1);

        long epochMilli = now.toEpochMilli();

//        now.isBefore()
//        now.isAfter()

    }
}
