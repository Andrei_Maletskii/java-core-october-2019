package lesson191111;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class PipedExamples {

    public static void main(String[] args) throws IOException {
        PipedReader pipedReader = new PipedReader();
        PipedWriter pipedWriter = new PipedWriter();
        pipedWriter.connect(pipedReader);

        pipedWriter.write("Hello");

        int i = 0;
        while ((i = pipedReader.read()) != -1) {
            System.out.println(i);
        }

        pipedWriter.close();
        pipedReader.close();
    }
}
