package lesson191111;

import java.io.File;
import java.io.IOException;

public class AdditionalFileMethodsExample {

    public static void main(String[] args) throws IOException {
        File currentDir = new File("./");
        File outputTxt = new File("output.txt");

        printCan(currentDir);
        System.out.println("Output before");
        printCan(outputTxt);
        chmod(outputTxt);
        System.out.println("Output after");
        printCan(outputTxt);

        System.out.println(new File("bytesToReadTest1").delete());
        new File("bytesToReadTest2").deleteOnExit();

        File tempFile = File.createTempFile("hello", "world");

        System.out.println(tempFile.getCanonicalPath());

        System.out.println("Exit");

    }

    private static void printCan(File file) throws IOException {
        System.out.println(file.getCanonicalPath());
        System.out.println(file.canExecute());
        System.out.println(file.canRead());
        System.out.println(file.canWrite());
    }

    private static void chmod(File file) {
        file.setExecutable(false);
        file.setReadable(false);
        file.setWritable(false);
    }
}
