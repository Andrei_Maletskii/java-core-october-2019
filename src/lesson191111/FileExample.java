package lesson191111;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

public class FileExample {
    public static void main(String[] args) throws IOException {
        File file = new File("../JavaOctober2019/bytesToRead");
        System.out.println(file.isFile());
        System.out.println(file.isDirectory());

        File directory = new File(".git");
        System.out.println(directory.isFile());
        System.out.println(directory.isDirectory());
        System.out.println(directory.isHidden());


        System.out.println("FILE bytesToRead");
        System.out.println(file.getName());
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getCanonicalPath());

        System.out.println(file.isAbsolute());
        System.out.println(file.isHidden());

        File nestedDir = new File("./folder4");

        System.out.println(nestedDir.isDirectory());
        System.out.println(nestedDir.mkdir());
        System.out.println(nestedDir.mkdirs());

        System.out.println("Current dir work -------");
        File currentDir = new File("./");
        System.out.println(currentDir.isDirectory());
        System.out.println(Arrays.toString(currentDir.list()));
        String[] startsWithFolder = currentDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("folder");
            }
        });
        System.out.println(Arrays.toString(startsWithFolder));

//        File[] files = currentDir.listFiles(new FileFilter() {
//            @Override
//            public boolean accept(File pathname) {
//                return pathname.isDirectory();
//            }
//        });


        File[] files = currentDir.listFiles(File::isDirectory);

        for (File file1 : files) {
            System.out.println(Arrays.toString(file1.list()));
        }



    }
}
