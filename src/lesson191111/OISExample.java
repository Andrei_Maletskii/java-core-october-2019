package lesson191111;

import java.io.*;

public class OISExample {

    public static void main(String[] args) {
        try (ObjectInputStream ois =
                     new ObjectInputStream(
                             new BufferedInputStream(
                                     new FileInputStream("byteOutput.bytes")
                             )
                     )
        ) {
            Object object = ois.readObject();
            if (object instanceof String) {
                String str = (String) object;

                System.out.println(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
