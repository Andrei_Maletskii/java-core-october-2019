package lesson191111;

import java.io.*;

public class OOSExamples {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("byteOutput.bytes");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        BufferedOutputStream bos =
                new BufferedOutputStream(fos);

        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(bos);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            oos.writeObject("Hello, world");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
