package lesson191111;

import java.io.*;
import java.util.Arrays;

public class ByteArrayExample {

    public static void main(String[] args) throws IOException {
        byte[] bytes = {-128, -50, -1, 0, 1, 50, 127};

        for (byte aByte : bytes) {
            System.out.println(aByte + " : " + Byte.toUnsignedInt(aByte));
        }

        writeToFile(bytes, "C:\\Users\\Andrei_Maletskii\\IdeaProjects\\bytesToRead");
        byte[] bytesToReads = readFromFile("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\bytesToRead");

        System.out.println(Arrays.toString(bytesToReads));

    }

    private static byte[] readFromFile(String fileName) {
        try (BufferedInputStream bis = new BufferedInputStream(
                new FileInputStream(fileName))
        ) {
            int availableBytes = bis.available();

            byte[] buffer = new byte[availableBytes];

            bis.read(buffer);

            return buffer;
        } catch (IOException e) {
            e.printStackTrace();

            throw new RuntimeException();
        }
    }

    private static void writeToFile(byte[] bytes, String fileName) {
        try (BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(fileName))
        ) {
            bos.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
