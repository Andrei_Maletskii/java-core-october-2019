package lesson191111;

import java.io.*;

public class SequenceInputStreamExample {

    public static void main(String[] args) throws IOException {
        FileInputStream fis1 = new FileInputStream("input1.txt");
        FileInputStream fis2 = new FileInputStream("input2.txt");
        SequenceInputStream sis = new SequenceInputStream(fis1, fis2);

        byte[] bytes = sis.readAllBytes();

        FileOutputStream fos = new FileOutputStream("sequenceOutput.txt");
        fos.write(bytes);

        sis.close();
        fos.close();
    }
}
