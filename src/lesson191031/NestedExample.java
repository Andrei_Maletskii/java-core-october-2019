package lesson191031;

public class NestedExample {

    private static int value;

    public static void method() {
        String str = Nested.str;
    }

    public static class Nested {

        private static String str;

        public void method() {
            System.out.println(NestedExample.value);
//            NestedExample nestedExample = NestedExample.this; ERROR
        }
    }
}
