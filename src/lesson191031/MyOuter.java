package lesson191031;

import java.lang.ref.PhantomReference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

public class MyOuter {

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Outer instance has been destroyed by gc");
    }

    public class MyInner {

    }

    public static void main(String[] args) throws InterruptedException {
        List<MyInner> list = new ArrayList<>();

        while (true) {
            MyInner e = new MyOuter().new MyInner();
            list.add(e);
            Thread.sleep(100);
            System.gc();

        }
//        WeakReference<List<MyInner>> listWeakReference = new WeakReference<>(list);

//        WeakReference weakReference;
//        SoftReference softReference;
//        PhantomReference phantomReference;
    }
}
