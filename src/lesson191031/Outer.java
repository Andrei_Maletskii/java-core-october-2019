package lesson191031;

public class Outer {
    private int value;

    public void method() {
        Inner inner = this.new InnerImpl(5);
    }

    public abstract class Inner {
        private int value;

        public Inner(int value) {
            this.value = value;
        }

        public void method() {
            System.out.println(this.value);
            System.out.println(Outer.this.value);
        }
    }

    public class InnerImpl extends Inner {

        public InnerImpl(int value) {
            super(value);
        }

        public Outer getOuter() {
            return Outer.this;
        }
    }

    public static void main(String[] args) {
        Outer outer = new Outer();

        Inner inner = outer.new InnerImpl(5);
        Inner inner1 = outer.new InnerImpl(10);
    }
}
