package lesson191031;

public class LocalClassExample {

    {
        class LocalInit {

        }

        LocalInit localInit = new LocalInit();
    }

    private int value;

    private void method1() {

    }

    public void method() {
        class Local extends LocalClassExample implements Runnable {
            private int value;

            private Local(int value) {
                this.value = value;
            }

            @Override
            public void method() {
                int value = LocalClassExample.this.value;
                LocalClassExample.this.method1();
                System.out.println(this.value);
            }

            @Override
            public void run() {

            }
        }

        Local local = new Local(5);
        local.method();
        int value = LocalClassExample.this.value;
    }

    public static void main(String[] args) {
        LocalClassExample localClassExample = new LocalClassExample();

//        Local local = localClassExample.new Local(5); ERROR
    }
}
