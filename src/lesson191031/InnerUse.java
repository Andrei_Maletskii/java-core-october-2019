package lesson191031;


import java.util.Map;
import java.util.Set;

public class InnerUse {

    public static void main(String[] args) {
        Outer outer = new Outer();

        Outer.InnerImpl inner = outer.new InnerImpl(5);

        Outer outer1 = inner.getOuter();

        Map<String, String> map = Map.of("key", "value");

        Set<Map.Entry<String, String>> entries = map.entrySet();

        for (Map.Entry<String, String> entry : entries) {
            String key = entry.getKey();
            String value = entry.getValue();
        }
    }
}
