package lesson191031;

public class AnonymousUseWithVariable {

    private int j = 0;
    private static int k = 0;

    private void method() {
        // final or effectively final
        int i = 0;
        Object object = new Object();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
//                System.out.println(object); ERROR
//                System.out.println(i);
                System.out.println(j);
                System.out.println(k);
            }
        };

        object = null;
        k = 3;
        j = 10;
        i = 5;
    }

    public static void main(String[] args) {

        for (int i = 0; i < 5; i++) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
//                    System.out.println(i); ERROR
                }
            };
        }

        final int i = 1;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println(i);
            }
        };
    }
}
