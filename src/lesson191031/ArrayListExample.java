package lesson191031;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {


    public static void main(String[] args) {

        List<String> strings = new ArrayList<>() {
            {
                add("str1");
                add("str2");
                add("str3");
            }
        };

        List<String> str1 = List.of("str1", "str2", "str3");
    }
}
