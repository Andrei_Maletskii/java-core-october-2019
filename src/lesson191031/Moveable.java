package lesson191031;

public interface Moveable {

    public abstract void method();
    void method1();

    default void method2() {
        method();
        System.out.println("hello");
        method3();
    }

    private void method3() {
        System.out.println();
    }

    private static Moveable getAnonymousInstance() {
        return new Moveable() {
            @Override
            public void method() {

            }

            @Override
            public void method1() {

            }
        };
    }

    public static Moveable getInstance() {
        return getInstance();
    }
}
