package lesson191031;

public class AnonymousExample {

    public static void main(String[] args) {

        X x = new X(3, 4) {
            @Override
            public void method() {
                int i = this.i;
                int k = this.k;
                int m = this.m;
//                int j = this.j; ERROR
                System.out.println("hi");
            }

            public void myMethod() {

            }
        };

//        x.myMethod();

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                System.out.println("hello");
//                hello();

                Runnable runnable1 = new Runnable() {

                    @Override
                    public void run() {

                    }
                };
            }
//
//            private void hello() {
//                System.out.println("hi");
//            }
        };

        Runnable runnable1 = () -> System.out.println("hello");

        runnable.run();

        x.method();

    }
}

class X {
    public int i = 0;
    protected int k = 0;
    int m = 0;
    private int j = 0;

    public X(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public void method() {
        System.out.println("hello");
    }
}
