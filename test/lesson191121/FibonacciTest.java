package lesson191121;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import java.math.BigInteger;

// TDD Test Driven Development
// 1. Write test
// 2. Launch test
// 3.1 If PASS - keep writing tests
// 3.2 IF FAILED - made smallest changes in the code

// 1 - Unit Testing
// 2 - Integration tests
// 3 - System testing
// 4 - E2E - End to end
// 5... Smoke Testing, Canary deployment....
public class FibonacciTest {

    private static Fibonacci fibonacciGenerator;

    @BeforeAll
    static void staticSetUp() {
        System.out.println("Creating Fibonacci...");
        fibonacciGenerator = new Fibonacci();
    }

    @BeforeEach
    void setUp(TestInfo testInfo) {
        System.out.println("Executing test " + testInfo.getDisplayName());
        fibonacciGenerator.clearCache();
    }

    @AfterEach
    void tearDown(TestInfo testInfo) {
        System.out.println("Test " + testInfo.getDisplayName() + " executed");
    }

    @AfterAll
    static void staticTearDown() {
        System.out.println("All tests executed");
    }

    @Test
    void shouldReturnZero() {
        BigInteger result = fibonacciGenerator.fibFast(0);

        Assertions.assertEquals(BigInteger.ZERO, result);
    }

    @Test
    void shouldReturnOne() {
        BigInteger result = fibonacciGenerator.fibFast(1);

        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturnOneForTwo() {
        BigInteger result = fibonacciGenerator.fibFast(2);

        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturn55For10() {
        BigInteger result = fibonacciGenerator.fibFast(10);
        BigInteger result1 = fibonacciGenerator.fibFast(9);

        Assertions.assertEquals(BigInteger.valueOf(55), result);
        Assertions.assertEquals(BigInteger.valueOf(34), result1);

    }

    @Test
    void shouldThrowAnIllegalArgumentException() {
        Assertions.assertThrows(Exception.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                fibonacciGenerator.fibFast(-1);
            }
        });
    }
}
